#include	"sys.h"
#include 	"TCProtocolManage.h"

#define		PACK_HEAD		0X55
#define		PACK_END		0XAA

MasterDataType s_MasterData;

MasterDataType* GetMasterData(void)
{
	return &s_MasterData;
}

//void STM32toMasterSendData(void)
//{
//	char *cJsonBuf, strTime[30];
//	static unsigned int Seq;
//	cJSON *Root, *Data;
//	u16 MsgLen = 0;
//	float Latitude = 0, Longitude = 0, X = 0, Y = 0, Z = 0;
//	int Temp = 0, f_Temp = 0;
//	ImuType *p_ImuDataOut = NULL;
//	RTKSTRUCT *p_RTKData = NULL;
//	RADARSTRUCT *p_RadarRange = NULL;

//	p_RadarRange = GetRange();
//	p_RTKData = GetRTKData();	
//	p_ImuDataOut = GetImuDataOut();
//	
//	if(p_RadarRange == NULL || p_RTKData == NULL  || p_ImuDataOut == NULL )
//	{
//		return;
//	}		
//	
//	Root = cJSON_CreateObject();	
//	
//	X = p_ImuDataOut->linear_acceleration_x;
//	Y = p_ImuDataOut->linear_acceleration_y;
//	Z = p_ImuDataOut->linear_acceleration_z;
//	
//	if( (fabs(Z) < 1) && (fabs(Y) < 1) && (fabs(X) < 1))
//	{
//		u1_printf("\r\n Imu Data 异常!!!\r\n");
//		return;
//	}
//	cJSON_AddNumberToObject(Root,"CMD", 		1);
//	cJSON_AddNumberToObject(Root,"Dir",			0);
//	cJSON_AddNumberToObject(Root,"Seq",			Seq++);
//	
//	cJSON_AddItemToObject(Root, "Msg", 		Data = cJSON_CreateObject());
//		
//	cJSON_AddNumberToObject(Data,"Vol",		GetDCPowerVol());
//	
//	cJSON_AddNumberToObject(Data,"R1",		p_RadarRange->Range1);
//	cJSON_AddNumberToObject(Data,"R2",		p_RadarRange->Range2);
//	cJSON_AddNumberToObject(Data,"R3",		p_RadarRange->Range3);
//	cJSON_AddNumberToObject(Data,"R4",		p_RadarRange->Range4);
//	
//	cJSON_AddNumberToObject(Data,"Lat",		p_RTKData->Latitude);
//	cJSON_AddNumberToObject(Data,"Lng",		p_RTKData->Longitude);
//	cJSON_AddNumberToObject(Data,"Sta",		p_RTKData->RTKStatus);
//	
//	Temp = (p_ImuDataOut->angular_x)*10;
//	cJSON_AddNumberToObject(Data,"Aglx",	Temp);
//	Temp = (p_ImuDataOut->angular_y)*10;
//	cJSON_AddNumberToObject(Data,"Agly",	Temp);
//	Temp = (p_ImuDataOut->angular_z)*10;
//	cJSON_AddNumberToObject(Data,"Aglz",	Temp);
//	
//	Temp = (p_ImuDataOut->linear_acceleration_x)*10;
//	cJSON_AddNumberToObject(Data,"Accx",	Temp);
//	Temp = (p_ImuDataOut->linear_acceleration_y)*10;
//	cJSON_AddNumberToObject(Data,"Accy",	Temp);
//	Temp = (p_ImuDataOut->linear_acceleration_z)*10;
//	cJSON_AddNumberToObject(Data,"Accz",	Temp);
//	
//	Temp = (p_ImuDataOut->angular_velocity_x)*10;
//	cJSON_AddNumberToObject(Data,"Avlx",	Temp);
//	Temp = (p_ImuDataOut->angular_velocity_y)*10;
//	cJSON_AddNumberToObject(Data,"Avly",	Temp);
//	Temp = (p_ImuDataOut->angular_velocity_z)*10;
//	cJSON_AddNumberToObject(Data,"Avlz",	Temp);
//	
//	f_Temp = (p_ImuDataOut->orientation_w);
//	cJSON_AddNumberToObject(Data,"W",	f_Temp);
//	f_Temp = (p_ImuDataOut->orientation_x);
//	cJSON_AddNumberToObject(Data,"X",	f_Temp);
//	f_Temp = (p_ImuDataOut->orientation_x);
//	cJSON_AddNumberToObject(Data,"Y",	f_Temp);
//	f_Temp = (p_ImuDataOut->orientation_x);
//	cJSON_AddNumberToObject(Data,"Z",	f_Temp);

//	cJSON_AddNumberToObject(Data,"Ratio",	p_ImuDataOut->MageneticRatio);
//	
//	cJSON_AddNumberToObject(Data,"Lev",	GetKeyStatus());
//	
//	cJsonBuf = cJSON_PrintUnformatted(Root);	
//	
//	MsgLen = strlen(cJsonBuf);
//		
//	cJSON_Delete(Root);	

//	printf("%s\r", cJsonBuf);
//	
//	if(GetShowDataFlag())
//		u1_printf("<- <- <- STM32toMasterSendData(Size:%d):%s\r\n", MsgLen, cJsonBuf);
//	
////	u1_printf(" Memory utilization:%d%%", mem_perused());	
//	
//	myfree(cJsonBuf);
//	
////	u1_printf(" -> -> %d%%\r\n\r\n", mem_perused());	
//}

int DecodecJsonMsg(char *cJsonBuf)
{
	cJSON *Root, *Temp, *Child, *tNode;
	cJSON *cjson_sub = NULL;
	cJSON *item =NULL;
	cJSON * cjson_arr = NULL;
	cJSON * cjson_arr_item = NULL;
	int cjson_arr_item_num = 0;
	//	 JSON_COMM_MSG cJsonMsg;
	char *Output, ArrayIP[4], strServer[120], Err;
	char strTime[30];
	unsigned char Cmd = 0, Dir = 0;
	unsigned int Seq = 0;
	unsigned char i, ConfigCount = 0;
	MasterDataType* p_MasterData;
	
	p_MasterData = GetMasterData();
	Root = cJSON_Parse((const char *)cJsonBuf);

	u1_printf("\n\nRecv cJson Packs(%d):%s\n", strlen(cJsonBuf) - 2,cJsonBuf);
	if (Root == NULL )
	{
	    u1_printf(" No cJson[%s]\n", cJSON_GetErrorPtr());
	    return -1;
	}

	Temp = cJSON_GetObjectItem(Root, "CMD");
	if(Temp != NULL)
	{
	    Cmd = Temp->valueint;
	}
	else
	{
	    return -1;
	}

	Temp = cJSON_GetObjectItem(Root, "Dir");
	if(Temp != NULL)
	{
	    Dir = Temp->valueint;
	}
	else
	{
	    return -1;
	}

	Temp = cJSON_GetObjectItem(Root, "Seq");
	if(Temp != NULL)
	{
	    Seq = Temp->valueint;
	}
	else
	{
	    return -1;
	}

    Child = cJSON_GetObjectItem(Root, "Msg");
    if((Child != NULL) && (Child->type == cJSON_Object))
    {
        tNode = cJSON_GetObjectItem(Child, "wlan");
        if(tNode != NULL)
        {
			if(strlen(tNode->valuestring) > 10)
			{
				memcpy(p_MasterData->WLAN_IP, tNode->valuestring, strlen(tNode->valuestring));
				u1_printf("WlanIP(%d):%s\r\n", strlen(tNode->valuestring), p_MasterData->WLAN_IP);
			}
			else
			{
				memset(p_MasterData->WLAN_IP, 0, sizeof(p_MasterData->WLAN_IP));
			}
        }
		
		tNode = cJSON_GetObjectItem(Child, "eth0");
        if(tNode != NULL)
        {
			if(strlen(tNode->valuestring) > 10)
			{
				memcpy(p_MasterData->LAN_IP, tNode->valuestring, strlen(tNode->valuestring));
				u1_printf("LANIP:%s\r\n", p_MasterData->LAN_IP);
			}
			else
			{
				memset(p_MasterData->LAN_IP, 0, sizeof(p_MasterData->LAN_IP));
			}
        }
		
//		tNode = cJSON_GetObjectItem(Child, "TFT");
//        if(tNode != NULL)
//        {
//			
//            if(tNode->valueint == 0)
//			{
//				SetTFT_Show_Flag(FALSE);
//			}
//			else
//			{
//				SetTFT_Show_Flag(TRUE);
//			}
//        }
    }

		
	 Output = cJSON_PrintUnformatted(Root);
	 cJSON_Delete(Root);
	 myfree(Output);

 	return 0;
 }
//void TaskForMasterCommunication(void)
//{
//	static unsigned char SendBuf[200] = {0},  Count = 0;
//	unsigned char CheckSum = 0, i;
//	float val_float = 0;
//	
//	ImuType* p_ImuDataOut;
//	
//	p_ImuDataOut = GetImuDataOut();
//	memset(SendBuf, 0 ,sizeof(SendBuf));
//	
//	CheckSum = 0;
//	Count = 0;
//	
//	SendBuf[Count++] = PACK_HEAD;
//	
//	val_float = encode_float(p_ImuDataOut -> angular_x);	
//	memcpy(&SendBuf[Count], (void *)&val_float, sizeof(float));
//	Count += 4;
//	val_float = encode_float(p_ImuDataOut -> angular_y);	
//	memcpy(&SendBuf[Count], (void *)&val_float, sizeof(float));
//	Count += 4;
//	val_float = encode_float(p_ImuDataOut -> angular_z);	
//	memcpy(&SendBuf[Count], (void *)&val_float, sizeof(float));
//	Count += 4;
//	
//	val_float = encode_float(p_ImuDataOut -> angular_velocity_x);	
//	memcpy(&SendBuf[Count], (void *)&val_float, sizeof(float));
//	Count += 4;
//	val_float = encode_float(p_ImuDataOut -> angular_velocity_y);	
//	memcpy(&SendBuf[Count], (void *)&val_float, sizeof(float));
//	Count += 4;
//	val_float = encode_float(p_ImuDataOut -> angular_velocity_z);	
//	memcpy(&SendBuf[Count], (void *)&val_float, sizeof(float));
//	Count += 4;
//	
//	val_float = encode_float(p_ImuDataOut -> linear_acceleration_x);	
//	memcpy(&SendBuf[Count], (void *)&val_float, sizeof(float));
//	Count += 4;
//	val_float = encode_float(p_ImuDataOut -> linear_acceleration_y);	
//	memcpy(&SendBuf[Count], (void *)&val_float, sizeof(float));
//	Count += 4;
//	val_float = encode_float(p_ImuDataOut -> linear_acceleration_z);	
//	memcpy(&SendBuf[Count], (void *)&val_float, sizeof(float));
//	Count += 4;
//	
//	val_float = encode_float(p_ImuDataOut -> MageneticRatio);	
//	memcpy(&SendBuf[Count], (void *)&val_float, sizeof(float));
//	Count += 4;
//	
//	SendBuf[Count++] = GetKeyStatus();
//	for(i=0; i<Count; i++) //加速度校验和
//	{
//		CheckSum += SendBuf[i];
//	}
//	
//	SendBuf[Count++] = CheckSum;
//	u1_u1_printf("Send Buf(%d):", Count);
//	for(i=0; i<Count; i++)
//	{
//		u1_u1_printf("%02X ", SendBuf[i]);
//	}
//	u1_u1_printf("\r\n");
//	USART2_DMA_Send(SendBuf, Count);		
//			
//}

//用于蓝牙交互
//void TaskForMasterRec(void)
//{
//	unsigned char Rec_Buf[100] = {0};
//	unsigned char CheckSum = 0;
//	static unsigned char Count = 0;
//	
//	if(g_Uart5RxFlag == TRUE)
//	{
//		if(g_USART5_RX_CNT < USART5_REC_LEN)
//		{		
////			u1_printf("Master2:%s\r\n", g_USART2_RX_BUF);
//			if((g_USART5_RX_BUF[0] == '{') && (g_USART5_RX_BUF[g_USART5_RX_CNT - 2] == '}'))
//			{
//				DecodecJsonMsg((char *)g_USART5_RX_BUF);
//			}			
//		}
//		Clear_Uart5Buff();
//	}
//}

//void MasterCommunicationInit(void)
//{
//	memset(&s_MasterData, 0, sizeof(s_MasterData));
//	
//	Task_Create(TaskForMasterRec, 1000);
//}

















