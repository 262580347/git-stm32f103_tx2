#include "sys.h"
#include "led_app.h"

#define		BLINK_TIME		10

const uint32_t RGBBuf[RGBMAX][PIXEL_NUM] = {
	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,
	0x00000001,	0x00000001,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,
	0x00000001,	0x00000001,	0x00000001,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,
	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000000,	0x00000000,	0x00000000,	0x00000000,
	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000000,	0x00000000,	0x00000000,
	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000000,	0x00000000,
	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000000,	0x00000000,
	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000000,
	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	
	
	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	
	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	
	
	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,
	0x00000000,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,
	0x00000000,	0x00000000,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,
	0x00000000,	0x00000000,	0x00000000,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,
	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000001,	0x00000001,	0x00000001,	0x00000001,
	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000001,	0x00000001,	0x00000001,
	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000001,	0x00000001,
	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000001,
	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000000
};

const uint32_t BrightBuf[1][PIXEL_NUM] = {
	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000001
};

static unsigned int s_LEDColor =  1;	//LED��ɫ 1~7��ʾ
static unsigned char s_LEDStatus =  LED_STATUS_BLINK;		//LED״̬ 1~5��ʾ
static unsigned char s_LEDBright = 1;	//LED����1-7


static unsigned char SPI_TxBuf[PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL];  

unsigned int GetLEDColor(void)
{
	return s_LEDColor;
}

void SetLEDColor(unsigned int Color)
{
	s_LEDColor = Color;
}

unsigned char GetLEDStatus(void)
{
	return s_LEDStatus;
}

void SetLEDStatus(unsigned char Status)
{
	s_LEDStatus = Status;
}

unsigned char GetLEDBright(void)
{
	return s_LEDBright;
}

void SetLEDBright(unsigned char Bright)
{
	s_LEDBright = Bright;
}

void TaskFor2812LED(void)
{
	static unsigned int s_Count = 0;
	static unsigned int s_LastStatus = 1, s_LastColor = 1, s_LastBright = 1, s_BlinkCount = 0;
	unsigned int i = 0, j = 0, RGBTemp = LED_COLOR_RED;
	static unsigned char s_BlinkStatus = 1;
	uint32_t OutBuf[RGBMAX][PIXEL_NUM] ;
	static int s_PWMBright = 0, s_Dir = 0;
	static unsigned char s_LEDColorTemp = 1;
	
	if(s_LEDBright < 1 || s_LEDBright > 7)
	{
		s_LEDBright = 1;
	}
	
	switch(s_LEDColor)	//��ɫ����ת��Ϊ����ɫ��ֵ
	{
		case 1:
			RGBTemp = LED_COLOR_RED*s_LEDBright;	
		break;
		
		case 2:
			RGBTemp = LED_COLOR_GREEN*s_LEDBright;	
		break;
		
		case 3:
			RGBTemp = LED_COLOR_BLUE*s_LEDBright;	
		break;
		
		case 4:
			RGBTemp = LED_COLOR_YELLOW*s_LEDBright;
		break;
		
		case 5:
			RGBTemp = LED_COLOR_CYAN*s_LEDBright;
		break;
		
		case 6:
			RGBTemp = LED_COLOR_PURPLE*s_LEDBright;	
		break;
		
		case 7:
			RGBTemp = LED_COLOR_WHITE*s_LEDBright;
		break;
		
		case 8:
			
			switch(s_LEDColorTemp)
			{
				case 1:
					RGBTemp = LED_COLOR_RED*s_LEDBright;	
				break;
				
				case 2:
					RGBTemp = LED_COLOR_GREEN*s_LEDBright;	
				break;
				
				case 3:
					RGBTemp = LED_COLOR_BLUE*s_LEDBright;	
				break;
				
				case 4:
					RGBTemp = LED_COLOR_YELLOW*s_LEDBright;
				break;
				
				case 5:
					RGBTemp = LED_COLOR_CYAN*s_LEDBright;
				break;
				
				case 6:
					RGBTemp = LED_COLOR_PURPLE*s_LEDBright;	
				break;
				
				case 7:
					RGBTemp = LED_COLOR_WHITE*s_LEDBright;
			break;
			}
			
			s_LEDColorTemp++;
			
			if(s_LEDColorTemp >= 8)
			{
				s_LEDColorTemp = 1;
			}
		break;
		
		default:
			RGBTemp = LED_COLOR_RED*s_LEDBright;
			s_LEDColor = 1;		
		break;
		
	}
				
	switch(s_LEDStatus)
	{
		case LED_STATUS_WATERFALL:		//��ˮ��
			
			s_LastStatus = s_LEDStatus;
			if(s_Count < RGBMAX)
			{
				
				for(i=0; i<RGBMAX; i++)
				{
					for(j=0; j<PIXEL_NUM; j++)
					{
						OutBuf[i][j] = RGBTemp*RGBBuf[i][j];
					}
				}
				
				WS2812_Pixels_Packet(PIXEL_NUM, (uint32_t *)OutBuf[s_Count], SPI_TxBuf);
				
				SPI_DMA_Enable(SPI1_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);

				SPI_DMA_Enable(SPI2_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);
			}
			
			s_Count++;
			if(s_Count >= RGBMAX+5)
			{
				s_Count = 0;

			}
		break;
		
		case LED_STATUS_BREATHING:		//������
			
			s_LastStatus = s_LEDStatus;
			if(s_Dir == 0)
			{
				if(s_PWMBright <= 40)
				{
					s_PWMBright += 2;
				}
				else
				{
					s_PWMBright += 8;	
				}
				
			
				if(s_PWMBright >= 0x00000100)
				{
					s_PWMBright = 0x000000ff;
					s_Dir = 1;
				}
			}
			
			if(s_Dir == 1)
			{
				if(s_PWMBright <= 40)
				{
					s_PWMBright -= 2;
				}
				else
				{
					s_PWMBright -= 8;	
				}
			
				if(s_PWMBright <= 0)
				{
					s_PWMBright = 0;
					s_Dir = 0;
				}
			}
						
			switch(s_LEDColor)
			{
				case 1:
					RGBTemp = s_PWMBright << 16;	
				break;
				
				case 2:
					RGBTemp = s_PWMBright << 8;	
				break;
				
				case 3:
					RGBTemp = s_PWMBright;	
				break;
				
				case 4:
					RGBTemp = (s_PWMBright << 16) | (s_PWMBright << 8);	
				break;
				
				case 5:
					RGBTemp = (s_PWMBright << 16) | (s_PWMBright);	
				break;
				
				case 6:
					RGBTemp = (s_PWMBright << 8) | (s_PWMBright);	
				break;
				
				case 7:
					RGBTemp = (s_PWMBright << 16) | (s_PWMBright << 8) | s_PWMBright;	
				break;
				
				default:
					RGBTemp = s_PWMBright;
				break;
			}

			for(i=0; i<PIXEL_NUM; i++)
			{
				OutBuf[0][i] = RGBTemp*BrightBuf[0][i];
			}
			
			WS2812_Pixels_Packet(PIXEL_NUM, (uint32_t *)OutBuf[0], SPI_TxBuf);
				
			SPI_DMA_Enable(SPI1_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);

			SPI_DMA_Enable(SPI2_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);
			
		break;
		
		case LED_STATUS_BLINK:			//��˸
			
			s_LastStatus = s_LEDStatus;
			if(s_BlinkStatus == 1)
			{
				for(i=0; i<PIXEL_NUM; i++)
				{
					OutBuf[0][i] = RGBTemp*BrightBuf[0][i];
				}		
				s_BlinkCount++;
				
				if(s_BlinkCount >= BLINK_TIME)
				{
					s_BlinkCount = 0;
					s_BlinkStatus = 0;
				}
				
			}
			else
			{
				memset(OutBuf, 0 ,sizeof(OutBuf));
				s_BlinkCount++;
				if(s_BlinkCount >= BLINK_TIME)
				{
					s_BlinkCount = 0;
					s_BlinkStatus = 1;
				}
			}
			
			WS2812_Pixels_Packet(PIXEL_NUM, (uint32_t *)OutBuf[0], SPI_TxBuf);
					
			SPI_DMA_Enable(SPI1_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);

			SPI_DMA_Enable(SPI2_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);
			
		break;
		
		case LED_STATUS_BRIGHT:			//����
			
			if((s_LastColor != s_LEDColor) || (s_LastStatus != s_LEDStatus) || (s_LastBright != s_LEDBright))
			{
				s_LastColor = s_LEDColor;
				s_LastStatus = s_LEDStatus;
				s_LastBright = s_LEDBright;
				for(i=0; i<PIXEL_NUM; i++)
				{
					OutBuf[0][i] = RGBTemp*BrightBuf[0][i];
				}
				WS2812_Pixels_Packet(PIXEL_NUM, (uint32_t *)OutBuf[0], SPI_TxBuf);
					
				SPI_DMA_Enable(SPI1_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);

				SPI_DMA_Enable(SPI2_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);
			}
			
		
		break;
		
		case LED_STATUS_LEFT:			//�����˸
			
			if(s_BlinkStatus == 1)
			{
				for(i=0; i<PIXEL_NUM; i++)
				{
					OutBuf[0][i] = RGBTemp*BrightBuf[0][i];
				}		
				s_BlinkCount++;
				
				if(s_BlinkCount >= BLINK_TIME)
				{
					s_BlinkCount = 0;
					s_BlinkStatus = 0;
				}
				
			}
			else
			{
				memset(OutBuf, 0 ,sizeof(OutBuf));
				s_BlinkCount++;
				if(s_BlinkCount >= BLINK_TIME)
				{
					s_BlinkCount = 0;
					s_BlinkStatus = 1;
				}
			}
			
			WS2812_Pixels_Packet(PIXEL_NUM, (uint32_t *)OutBuf[0], SPI_TxBuf);
					
			SPI_DMA_Enable(SPI1_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);

//			SPI_DMA_Enable(SPI2_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);
			
			if(s_LastStatus != s_LEDStatus)
			{
				s_LastStatus = s_LEDStatus;
				
				memset(OutBuf, 0 ,sizeof(OutBuf));
					
				WS2812_Pixels_Packet(PIXEL_NUM, (uint32_t *)OutBuf[0], SPI_TxBuf);
					
				SPI_DMA_Enable(SPI2_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);
				
			}
			
		break;
			
		case LED_STATUS_RIGHT:			//�ҵ���˸
			
			if(s_BlinkStatus == 1)
			{
				for(i=0; i<PIXEL_NUM; i++)
				{
					OutBuf[0][i] = RGBTemp*BrightBuf[0][i];
				}		
				s_BlinkCount++;
				
				if(s_BlinkCount >= BLINK_TIME)
				{
					s_BlinkCount = 0;
					s_BlinkStatus = 0;
				}
				
			}
			else
			{
				memset(OutBuf, 0 ,sizeof(OutBuf));
				s_BlinkCount++;
				if(s_BlinkCount >= BLINK_TIME)
				{
					s_BlinkCount = 0;
					s_BlinkStatus = 1;
				}
			}
			
			WS2812_Pixels_Packet(PIXEL_NUM, (uint32_t *)OutBuf[0], SPI_TxBuf);
					
//			SPI_DMA_Enable(SPI1_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);

			SPI_DMA_Enable(SPI2_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);
			
			if(s_LastStatus != s_LEDStatus)
			{
				s_LastStatus = s_LEDStatus;
				
				memset(OutBuf, 0 ,sizeof(OutBuf));
					
				WS2812_Pixels_Packet(PIXEL_NUM, (uint32_t *)OutBuf[0], SPI_TxBuf);
					
				SPI_DMA_Enable(SPI1_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);
				
			}
			
		break;
			
		case LED_STATUS_POWEROFF:		//Ϩ��
			
			if(s_LastStatus != s_LEDStatus)
			{
				s_LastStatus = s_LEDStatus;
				
				memset(OutBuf, 0 ,sizeof(OutBuf));
				
				WS2812_Pixels_Packet(PIXEL_NUM, (uint32_t *)OutBuf[0], SPI_TxBuf);
					
				SPI_DMA_Enable(SPI1_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);

				SPI_DMA_Enable(SPI2_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);
			}
		break;
			
		default:
			s_LEDStatus = LED_STATUS_WATERFALL;
		break;
			
	}
}

void LEDPowerOff(void)
{
	uint32_t OutBuf[RGBMAX][PIXEL_NUM] ;
	
	memset(OutBuf, 0 ,sizeof(OutBuf));
				
	WS2812_Pixels_Packet(PIXEL_NUM, (uint32_t *)OutBuf[0], SPI_TxBuf);
		
	SPI_DMA_Enable(SPI1_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);

	SPI_DMA_Enable(SPI2_TX_DMA_CHANNEL, PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);
}
void WS2812_Port_Init(void)
{
	u1_printf("WS2812 SPI Init Start\r\n");
	SPI1_Init();
	SPI2_Init();
	SPI_DMA_Config(SPI1_TX_DMA_CHANNEL, (uint32_t)&SPI1->DR,(uint32_t)SPI_TxBuf,PIXEL_NUM * 24 * SPI1_BYTE_LEN_PER_PIXEL);
	SPI_DMA_Config(SPI2_TX_DMA_CHANNEL, (uint32_t)&SPI2->DR,(uint32_t)SPI_TxBuf,PIXEL_NUM * 24 * SPI2_BYTE_LEN_PER_PIXEL);
	SPI2_DMA_IT_Config();
	
	u1_printf("WS2812 SPI Init Finish\r\n");
}


































