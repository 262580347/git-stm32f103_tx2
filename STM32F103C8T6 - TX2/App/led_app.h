#ifndef		_LED_APP_H_
#define		_LED_APP_H_

#define		LED_STATUS_WATERFALL	1	//��ˮ��
#define		LED_STATUS_BREATHING	2	//������
#define		LED_STATUS_BLINK		3	//��˸
#define		LED_STATUS_BRIGHT		4	//����
#define		LED_STATUS_LEFT			5	//�����
#define		LED_STATUS_RIGHT		6	//�ҵ���
#define		LED_STATUS_POWEROFF		7	//Ϩ��

#define		LED_COLOR_RED		0x00200000		//��	
#define		LED_COLOR_GREEN		0x00002000		//��
#define		LED_COLOR_BLUE		0x00000020		//��
#define		LED_COLOR_YELLOW	0x00202000		//��
#define		LED_COLOR_CYAN		0x00200020		//��
#define		LED_COLOR_PURPLE	0x00002020		//��
#define		LED_COLOR_WHITE		0x00202020		//��


void TaskFor2812LED(void);

void WS2812_Port_Init(void);

unsigned int GetLEDColor(void);

void SetLEDColor(unsigned int Color);

unsigned char GetLEDStatus(void);

void SetLEDStatus(unsigned char Status);

unsigned char GetLEDBright(void);

void SetLEDBright(unsigned char Bright);

void LEDPowerOff(void);








#endif




