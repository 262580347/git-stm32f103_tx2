#ifndef		_CANCOMMUNICATION_H_
#define		_CANCOMMUNICATION_H_

#define		MCU1_ID		0xA0

#define		IMU_DATA_TYPE			1		//float
#define		RTK_DATA_TYPE			2		//float
#define		RADAR_DATA_TYPE			3		//int
#define		VOL_DATA_TYPE			4		//int
#define		LEVEL_STATUS_TYPE		5		//U8
#define		POWER_STATUS_TYPE		6		//U8
#define		TFT_STATUS_TYPE			7		//U8
#define		POWER_CONTROL_TYPE		8		//6路电源输出控制 08 01 01 01 00 00 00 check	01为开 00 为关
#define		LED_STATUS_TYPE			9		//LED状态数据  颜色 状态 亮度

#define		MPU_GYRO				10		//陀螺仪原始数据
#define		MPU_AAC					11		//重力传感器数据
#define		MPU_EULER				12		//欧拉角
#define		MPU_RESET				13		//重制MPU数据

#define		DATA_TYPE_FLOAT			1
#define		DATA_TYPE_INT			2
#define		DATA_TYPE_SHORT			3
#define		DATA_TYPE_U8			4

#include "DataType.h"

void CanCommunicationInit(void);

void CanProtocolSend(unsigned char SensorType, unsigned char Index, unsigned char DataType, void* Data);

RADARSTRUCT *GetRange(void);

unsigned char GetCanRecFlag(void);

unsigned char GetCanSendFlag(void);


#endif



