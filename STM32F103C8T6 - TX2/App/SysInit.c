#include "sys.h"
#include "SysInit.h"

#define IWDG_OVER_TIMER   26

static unsigned char s_SystemStatus = SYS_INIT, s_TX2Status = SYS_TX2_POWER_OFF;

unsigned char GetSystemStatus(void)
{
	return s_SystemStatus;
}

void SetSystemStatus(unsigned char Status)
{
	s_SystemStatus = Status;
}

unsigned char GetTX2Status(void)
{
	return s_TX2Status;
}

void SetTX2Status(unsigned char Status)
{
	s_TX2Status = Status;
}

void Updata_IWDG(void)
{
	IWDG_ReloadCounter();
}

void SetIWDG(unsigned int msTime)
{
	if(msTime>IWDG_OVER_TIMER)
	{
		msTime = IWDG_OVER_TIMER;
	}
   	if(msTime>0)
	{
		IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
		IWDG_SetPrescaler(IWDG_Prescaler_256);
		IWDG_SetReload(156*msTime);  //Max: 0xfff
		IWDG_ReloadCounter();
		IWDG_Enable();	
	}
	
	Task_Create(Updata_IWDG, 40);
}

void SysInit(void)
{	
	int ret = 0;
	
	RCC_ClocksTypeDef RCC_Clocks;
	
	RCC_GetClocksFreq(&RCC_Clocks);

	delay_init();	    	            //=====延时函数初始化	
	
	JTAG_Set(SWD_ENABLE);           //=====打开SWD接口 可以利用主板的SWD接口调试
	
	MY_NVIC_PriorityGroupConfig(2);	//=====设置中断分组
	
	LED_Init();                     //=====初始化与 LED 连接的硬件接口
	
	KEY_Init();
	
	Bumper_Sensor_GPIO_Init();
	
	PowerControlPortInit();
		
	Uart1_Init(115200);   

	TIM3_Init(9999, 71);
			
	CanCommunicationInit();
	
	Adc_Init();
	
	SetIWDG(5);
	
	MPU_Init();
	
	while(mpu_dmp_init())
	{
		delay_ms(1);
	}
	
	WS2812_Port_Init();
	
	u1_printf(" STM32F103RCT6 Run... TX2 Sys Clock:%dHz\r\n", RCC_Clocks.SYSCLK_Frequency);
	
	BeepPortInit();
	
	LEDPowerOff();

}


