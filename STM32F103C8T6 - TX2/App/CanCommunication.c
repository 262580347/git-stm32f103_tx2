 /*
*	CAN通信数据格式：标志CAN 8个字节
*		1				2		3			4		5		6		7		8
* 	数据类型		   索引		数据格式			数据（4字节）			校验和
*	20200704	作者：关宇晟
*/
#include "sys.h"
#include "CanCommunication.h"


#define		MCU2_ID		0xA2
#define		CAN_LEN		8

static RADARSTRUCT s_RadarRange;

RADARSTRUCT *GetRange(void)
{
	return &s_RadarRange;
}

static unsigned char s_CanRecFlag = FALSE;

static unsigned char s_CanSendFlag = FALSE;

unsigned char GetCanRecFlag(void)	//CAN发送成功标志
{
	return s_CanRecFlag;
}

unsigned char GetCanSendFlag(void)	//CAN发送成功标志
{
	return s_CanSendFlag;
}
//发送陀螺仪原始数据	
void CanSendGyroData(void)
{
	unsigned char i, CanBuf[8] = {0}, CheckSum = 0;
	u8 ret  =0;
	POWERSTATUS *PowerStatus;
	
	PowerStatus = GetPowerStatus();
	
	CanBuf[0] = MPU_GYRO;
	CanBuf[1] = g_gyrox >> 8;
	CanBuf[2] = g_gyrox&0xff;
	CanBuf[3] = g_gyroy >> 8;
	CanBuf[4] = g_gyroy&0xff;
	CanBuf[5] = g_gyroz >> 8;
	CanBuf[6] = g_gyroz&0xff;

	CheckSum = 0;
	for(i=0; i<CAN_LEN-1; i++)
	{
		CheckSum += CanBuf[i];
	}
	CanBuf[7] = CheckSum;

	ret = Can_Send_Msg(MCU1_ID, CanBuf, CAN_LEN); 
}
//发送陀螺仪加速度数据
void CanSendAacData(void)
{
	unsigned char i, CanBuf[8] = {0}, CheckSum = 0;
	u8 ret  =0;

	CanBuf[0] = MPU_AAC;
	CanBuf[1] = g_aacx >> 8;
	CanBuf[2] = g_aacx&0xff;
	CanBuf[3] = g_aacy >> 8;
	CanBuf[4] = g_aacy&0xff;
	CanBuf[5] = g_aacz >> 8;
	CanBuf[6] = g_aacz&0xff;

	CheckSum = 0;
	for(i=0; i<CAN_LEN-1; i++)
	{
		CheckSum += CanBuf[i];
	}
	CanBuf[7] = CheckSum;

	ret = Can_Send_Msg(MCU1_ID, CanBuf, CAN_LEN); 
}
//发送陀螺仪欧拉角数据
void CanSendEulerData(void)
{
	short pitch, roll, yaw;
	unsigned char i, CanBuf[8] = {0}, CheckSum = 0;
	u8 ret  =0;
	
	pitch = (short)(g_pitch*10);
	roll = (short)(g_roll*10);
	yaw = (short)(g_yaw*10);
	
	CanBuf[0] = MPU_EULER;
	CanBuf[1] = pitch >> 8;
	CanBuf[2] = pitch&0xff;
	CanBuf[3] = roll >> 8;
	CanBuf[4] = roll&0xff;
	CanBuf[5] = yaw >> 8;
	CanBuf[6] = yaw&0xff;

	CheckSum = 0;
	for(i=0; i<CAN_LEN-1; i++)
	{
		CheckSum += CanBuf[i];
	}
	CanBuf[7] = CheckSum;

	ret = Can_Send_Msg(MCU1_ID, CanBuf, CAN_LEN); 
}


void CanSendPowerStatus(void)
{
	unsigned char i, CanBuf[8] = {0}, CheckSum = 0;
	u8 ret  =0;
	POWERSTATUS *PowerStatus;
	
	PowerStatus = GetPowerStatus();
	
	CanBuf[0] = POWER_CONTROL_TYPE;
	CanBuf[1] = PowerStatus->PowerStatus1;
	CanBuf[2] = PowerStatus->PowerStatus2;
	CanBuf[3] = PowerStatus->PowerStatus3;
	CanBuf[4] = PowerStatus->PowerStatus4;
	CanBuf[5] = PowerStatus->PowerStatus5;
	CanBuf[6] = PowerStatus->PowerStatus6;

	CheckSum = 0;
	for(i=0; i<CAN_LEN-1; i++)
	{
		CheckSum += CanBuf[i];
	}
	CanBuf[7] = CheckSum;

	ret = Can_Send_Msg(MCU1_ID, CanBuf, CAN_LEN); 
}

void CanSendLEDStatus(void)
{
	unsigned char i, CanBuf[8] = {0}, CheckSum = 0;
	unsigned char LEDColor = 1, LEDStatus = 1, LEDBright = 1;
	int ret = 0;
	static unsigned char s_First = FALSE;
	
	LEDColor = GetLEDColor();
	LEDStatus = GetLEDStatus();
	LEDBright = GetLEDBright();
	
	CanBuf[0] = LED_STATUS_TYPE;
	CanBuf[1] = LEDColor;
	CanBuf[2] = LEDStatus;
	CanBuf[3] = LEDBright;
	CanBuf[4] = GetBumperLeftFlag();	//左碰撞
	CanBuf[5] = GetBumperRightFlag();	//右碰撞
	CanBuf[6] = 0;	//保留位

	CheckSum = 0;
	for(i=0; i<CAN_LEN-1; i++)
	{
		CheckSum += CanBuf[i];
	}
	CanBuf[7] = CheckSum;

	ret = Can_Send_Msg(MCU1_ID, CanBuf, CAN_LEN); 
	
	if(ret != 255)
	{
//		u1_printf("Fail:%d\r\n", ret);
		s_CanSendFlag = FALSE;
	}
	else
	{
		s_CanSendFlag = TRUE;
		if(s_First == FALSE)//CAN总线发送成功后切换一次LED灯状态
		{
			s_First = TRUE;
			SetLEDColor(2);
			SetLEDStatus(1);
		}
	}
}

void CanProtocolSend(unsigned char SensorType, unsigned char Index, unsigned char DataType, void* Data)
{
	unsigned char i, CanBuf[8] = {0};

	unsigned char  CheckSum = 0;

	
	CanBuf[0] = SensorType;
	CanBuf[1] = Index;
	CanBuf[2] = DataType;
	
	switch(DataType)
	{
		case DATA_TYPE_FLOAT:
			memcpy(&CanBuf[3], (void *)Data, sizeof(float));			
		break;
		
		case DATA_TYPE_INT:
			memcpy(&CanBuf[3], (void *)Data, sizeof(int));		
		break;
		
		case DATA_TYPE_U8:
			CanBuf[3] = 0;
			CanBuf[4] = 0;
			CanBuf[5] = 0;
			CanBuf[6] = *(unsigned char *)(Data);
		break;
	}
	
	CheckSum = 0;
	for(i=0; i<CAN_LEN-1; i++)
	{
		CheckSum += CanBuf[i];
	}
	CanBuf[7] = CheckSum;
//	u1_printf("Send Buf:");
//	for(i=0; i<8; i++)
//	{
//		u1_printf("%02X ", CanBuf[i]);
//	}
//	u1_printf("\r\n");
	Can_Send_Msg(MCU1_ID, CanBuf, CAN_LEN); 
}

void TaskForCanReceive(void)
{
	static unsigned int s_LastTime = 0;
	unsigned int ID = 0, SensorID = 0;
	unsigned char RecBuf[8] = {0}, i, CheckSum = 0;


	int val_int = 0;
	unsigned char LEDColor = 0, LEDStatus = 0, LEDBright = 0;
	POWERSTATUS *PowerStatus;
	
	ID = Can_Receive_Msg(RecBuf);
	
	if(CalculateTime(GetSystem100msCount(), s_LastTime) >= 20 && s_CanRecFlag == TRUE)	//2s内没有收到Can总线上的数据
	{
		s_LastTime = GetSystem100msCount();
		s_CanRecFlag = FALSE;
		u1_printf("超过2s没有收到CAN数据\r\n");
	}
	
	if(ID)
	{
		s_CanRecFlag = TRUE;
		s_LastTime = GetSystem100msCount();
		CheckSum = 0;
		for(i=0;i<7;i++)
		{
			CheckSum += RecBuf[i];
		}	
		
		if(ID == 131 && ID == 141 && ID == 151 && ID == 200 && ID == 201 && ID == 202 && ID == 203)	
		{
			return;
		}
//		else
//		{
//			u1_printf("(ID 0x%04X) Rec:", ID);	
//			for(i=0;i<8;i++)
//			{
//				u1_printf("%02X ", RecBuf[i]);
//			}	
//			u1_printf("\r\n");
//		}
		
		if(CheckSum == RecBuf[7])
		{
			SensorID  = RecBuf[0];
			switch(SensorID)
			{
				case TFT_STATUS_TYPE:	//外部输出控制

				break;
					
				case VOL_DATA_TYPE:	//外部电压
					memcpy(&val_int, (void *)&RecBuf[3], sizeof(int));
					SetDCPowerVol(val_int);
//					u1_printf(" DC Power vol:%d\r\n", val_int);
				break;
				
				case RTK_DATA_TYPE:	//RTK数据

				break;
				
				case POWER_CONTROL_TYPE:
					PowerStatus = GetPowerStatus();
					PowerStatus->PowerStatus1 = RecBuf[1];
					PowerStatus->PowerStatus2 = RecBuf[2];
					PowerStatus->PowerStatus3 = RecBuf[3];
					PowerStatus->PowerStatus4 = RecBuf[4];
					PowerStatus->PowerStatus5 = RecBuf[5];
					PowerStatus->PowerStatus6 = RecBuf[6];
				
					u1_printf("Power Status:%d %d %d %d %d %d\r\n", RecBuf[1], RecBuf[2], RecBuf[3], RecBuf[4], RecBuf[5], RecBuf[6]);
					BeepRangOnce();
				break;
				
				case LED_STATUS_TYPE:
					
					LEDColor = RecBuf[1];
					LEDStatus = RecBuf[2];
					LEDBright = RecBuf[3];
					u1_printf("LED: Color:%d Status:%d Bright:%d\r\n", LEDColor, LEDStatus, LEDBright);	
					SetLEDColor(LEDColor);
					SetLEDStatus(LEDStatus);		
					SetLEDBright(LEDBright);
					
				break;
				
				case MPU_RESET:
					MPU6050_Reset();
				break;
				
				default:
					
				break;
			}
		}
		else
		{
//			u1_printf("CheckSum Err,(ID 0x%04X) Rec:", ID);	
//			for(i=0;i<8;i++)
//			{
//				u1_printf("%02X ", RecBuf[i]);
//			}	
//			u1_printf("\r\n");
		}
	}
}

void TaskForCanCSend(void)
{
	int DCPowerVol = 0;
	static unsigned int RunCnt = 0;

	DCPowerVol = GetDCPowerVol();
	
	if(RunCnt >= 50)
	{
		CanSendLEDStatus();

		if(s_CanSendFlag == TRUE)
		{
			CanProtocolSend(VOL_DATA_TYPE, 0, DATA_TYPE_INT, (void *)&DCPowerVol);

			CanSendPowerStatus();
		}
		
		RunCnt = 0;
	}
	else if(s_CanSendFlag == TRUE)
	{
		CanSendGyroData();
	
		CanSendAacData();

		CanSendEulerData();
	}
	RunCnt++;
//	u1_printf("Can Send\r\n");
	
}

void CanCommunicationInit(void)
{
	CAN_Mode_Init(4, CAN_Mode_Normal);			//=====CAN初始化,波特率500Kbps  36M/((1+9+8)*X)=2/X=0.5M   CAN_Mode_Normal   CAN_Mode_LoopBack
	
	Task_Create(TaskForCanReceive, 1);
	
//	Task_Create(TaskForCanCSend, 30000);
	SetTimer(200, TaskForCanCSend); 
	
	u1_printf(" Can Communication Init Finish..\r\n");
}
