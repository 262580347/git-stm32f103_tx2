#ifndef  _SYS_INIT_
#define	 _SYS_INIT_

#define		SYS_INIT					0
#define		SYS_NORMAL					1
#define		SYS_TX2_POWER_ON			2
#define		SYS_TX2_POWER_OFF			3
#define		SYS_SHUTINGDOWN				4	//���ڹػ�


void SysInit(void);

unsigned char GetSystemStatus(void);

void SetSystemStatus(unsigned char Status);

unsigned char GetTX2Status(void);

void SetTX2Status(unsigned char Status);
	
#endif
