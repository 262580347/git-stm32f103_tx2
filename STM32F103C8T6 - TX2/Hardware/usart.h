#ifndef __USART_H
#define __USART_H

#include "sys.h" 

#define 	USART_REC_LEN  					500  
#define 	USART2_REC_LEN  				500  
#define 	USART3_REC_LEN  				500  
#define 	USART4_REC_LEN  				500  
#define 	USART5_REC_LEN  				500 

	  	
extern unsigned char   g_UartRxFlag;					//串口1接收完标志
extern unsigned char  	g_USART_RX_BUF[USART_REC_LEN];	//串口1缓冲区
extern unsigned short int 	g_USART_RX_CNT;					//串口1消息长度

//extern unsigned char   g_Uart2RxFlag;					//串口1接收完标志
//extern unsigned char  	g_USART2_RX_BUF[USART2_REC_LEN];	//串口1缓冲区
//extern unsigned short int 	g_USART2_RX_CNT;					//串口1消息长度

//extern unsigned char   g_Uart3RxFlag;					//串口1接收完标志
//extern unsigned char  	g_USART3_RX_BUF[USART3_REC_LEN];	//串口1缓冲区
//extern unsigned short int 	g_USART3_RX_CNT;					//串口1消息长度

//extern unsigned char   g_Uart4RxFlag;					//串口1接收完标志
//extern unsigned char  	g_USART4_RX_BUF[USART4_REC_LEN];	//串口1缓冲区
//extern unsigned short int 	g_USART4_RX_CNT;					//串口1消息长度

//extern unsigned char   g_Uart5RxFlag;					//串口2接收完标志
//extern unsigned char  	g_USART5_RX_BUF[USART5_REC_LEN];	//串口2缓冲区
//extern unsigned short int 	g_USART5_RX_CNT;					//串口2消息长度



void Uart1_Init(u32 bound);
void Uart2_Init(u32 bound);
void Uart3_Init(u32 bound);
void Uart4_Init(u32 bound);
void Uart5_Init(u32 bound);

void Clear_Uart1Buff(void);
void Clear_Uart2Buff(void);
void Clear_Uart3Buff(void);
void Clear_Uart4Buff(void);
void Clear_Uart5Buff(void);

void Uart5_Send(unsigned char *buf, unsigned short len);

#endif


