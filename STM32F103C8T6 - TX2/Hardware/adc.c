#include "adc.h"

static int DC_PowerVol = 0, TX2_PowerVol = 0;

int GetDCPowerVol(void)
{
	return DC_PowerVol;
}

void SetDCPowerVol(int Vol)
{
	DC_PowerVol = Vol;
}

int GetTX2PowerVol(void)
{
	return TX2_PowerVol;
}

void SetTX2PowerVol(int Vol)
{
	TX2_PowerVol = Vol;
}

/**************************************************************************
函数功能：AD采样
入口参数：ADC1 的通道
返回  值：AD转换结果
**************************************************************************/
u16 Get_Adc(u8 ch)   
{
	  	//设置指定ADC的规则组通道，一个序列，采样时间
	ADC_RegularChannelConfig(ADC1, ch, 1, ADC_SampleTime_239Cycles5 );	//ADC1,ADC通道,采样时间为239.5周期	  			     
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);		//使能指定的ADC1的软件转换启动功能		 
	while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC ));//等待转换结束
	return ADC_GetConversionValue(ADC1);	//返回最近一次ADC1规则组的转换结果
}


/**************************************************************************
函数功能：读取电池电压 
入口参数：无
返回  值：电池电压 单位MV
**************************************************************************/
int Get_battery_volt(u8 ch)   
{  
	int i = 0;
	static int Volt = 0;//电池电压
	
	Volt = Get_Adc(ch);
	return Volt;
}

void TaskForDetectVol(void)
{
	static unsigned char s_LastStatus = SYS_TX2_POWER_OFF;
	static unsigned int Count = 0;
	
	DC_PowerVol = Get_battery_volt(BAT_CH);
	DC_PowerVol = DC_PowerVol*8.864469;
	
	
	TX2_PowerVol = Get_battery_volt(TX2_CH);
	TX2_PowerVol = TX2_PowerVol*0.806;
	
	
	if(TX2_PowerVol > 500)
	{
		SetTX2Status(SYS_TX2_POWER_OFF);
//		u1_printf("TX2 未启动\r\n");
		
		if(s_LastStatus != GetTX2Status())
		{
			s_LastStatus = GetTX2Status();
			BeepRangTwice();
		}
	}
	else
	{
		
		SetTX2Status(SYS_TX2_POWER_ON);
//		u1_printf("TX2 已开启\r\n");
//		
		if(s_LastStatus != GetTX2Status())
		{
			s_LastStatus = GetTX2Status();
			BeepRangTwice();
		}
	}
	
	Count++;
	
	if(Count >= 30)
	{
		Count = 0;
		u1_printf("DC Power:%dmV  ", DC_PowerVol);
		u1_printf("TX2 Vol:%dmV  ", TX2_PowerVol);
	}
}

/**************************************************************************
函数功能：ACD初始化电池电压检测
入口参数：无
返回  值：无
作    者：平衡小车之家
**************************************************************************/
void  Adc_Init(void)
{    
 	ADC_InitTypeDef ADC_InitStructure; 
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC |RCC_APB2Periph_ADC1 | RCC_APB2Periph_ADC2 , ENABLE );	  //使能ADC1通道时钟
	RCC_ADCCLKConfig(RCC_PCLK2_Div6);   //设置ADC分频因子6 72M/6=12,ADC最大时间不能超过14M
	//设置模拟通道输入引脚                         
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;		//模拟输入引脚
	GPIO_Init(GPIOB, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;		//模拟输入引脚
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	ADC_DeInit(ADC1);  //复位ADC1,将外设 ADC1 的全部寄存器重设为缺省值
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;	//ADC工作模式:ADC1和ADC2工作在独立模式
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;	//模数转换工作在单通道模式
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;	//模数转换工作在单次转换模式
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;	//转换由软件而不是外部触发启动
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;	//ADC数据右对齐
	ADC_InitStructure.ADC_NbrOfChannel = 1;	//顺序进行规则转换的ADC通道的数目
	ADC_Init(ADC1, &ADC_InitStructure);	//根据ADC_InitStruct中指定的参数初始化外设ADCx的寄存器   
	ADC_Cmd(ADC1, ENABLE);	//使能指定的ADC1
	ADC_ResetCalibration(ADC1);	//使能复位校准  	 
	while(ADC_GetResetCalibrationStatus(ADC1));	//等待复位校准结束	
	ADC_StartCalibration(ADC1);	 //开启AD校准
	while(ADC_GetCalibrationStatus(ADC1));	 //等待校准结束
	
	Task_Create(TaskForDetectVol, 200000);
}		





