#ifndef		_DMA_H_
#define		_DMA_H_


void DMA_Uart1_Config(void);
void DMA_Uart2_Config(void);
void DMA_Uart3_Config(void);
void DMA_Uart4_Config(void);
void DMA_Uart5_Config(void);


void u1_printf(char* fmt,...);
void u2_printf(char* fmt,...);
void u3_printf(char* fmt,...);
void u4_printf(char* fmt,...);
void u5_printf(char* fmt,...);


void USART1_DMA_Send(unsigned char *buffer, unsigned short buffer_size);
void USART2_DMA_Send(unsigned char *buffer, unsigned short buffer_size);
void USART3_DMA_Send(unsigned char *buffer, unsigned short buffer_size);
void UART4_DMA_Send(unsigned char *buffer, unsigned short buffer_size);
void UART5_DMA_Send(unsigned char *buffer, unsigned short buffer_size);

#endif


