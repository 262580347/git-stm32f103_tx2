#include "sys.h"
#include "usart.h"	

u8 		g_USART_RX_BUF[USART_REC_LEN] ;
u16 	g_USART_RX_CNT=0;	
u8 		g_UartRxFlag = FALSE;

//u8 		g_USART2_RX_BUF[USART2_REC_LEN] ;
//u16 	g_USART2_RX_CNT=0;	
//u8 		g_Uart2RxFlag = FALSE;

//u8 		g_USART3_RX_BUF[USART3_REC_LEN] ;
//u16 	g_USART3_RX_CNT=0;	
//u8 		g_Uart3RxFlag = FALSE;

//u8 		g_USART4_RX_BUF[USART4_REC_LEN] ;
//u16 	g_USART4_RX_CNT=0;	
//u8 		g_Uart4RxFlag = FALSE;

//u8 		g_USART5_RX_BUF[USART5_REC_LEN] ;
//u16 	g_USART5_RX_CNT=0;	
//u8 		g_Uart5RxFlag = FALSE;

//加入以下代码,支持printf函数,而不需要选择use MicroLIB	  
#if 1
#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE 
{ 
	int handle; 
}; 

FILE __stdout;       
//定义_sys_exit()以避免使用半主机模式    
void _sys_exit(int x) 
{ 
	x = x; 
} 
//重定义fputc函数 
int fputc(int ch, FILE *f)
{ 	
	while((UART5->SR&0X40)==0);//Flag_Show=0  使用串口3
	UART5->DR = (u8) ch;      
	return ch;
}
#endif
 

void Uart5_Send(unsigned char *buf, unsigned short len)
{
	int i = 0;
	
	while(len--)
	{
		while((UART5->SR&0X40)==0);//Flag_Show=0  使用串口3
		UART5->DR = buf[i++];    
	}		
}
void Clear_Uart1Buff(void)
{
	memset(g_USART_RX_BUF, 0, USART_REC_LEN);
	g_USART_RX_CNT = 0;
	g_UartRxFlag = FALSE;
}

//void Clear_Uart2Buff(void)
//{
//	memset(g_USART2_RX_BUF, 0, USART2_REC_LEN);
//	g_USART2_RX_CNT = 0;
//	g_Uart2RxFlag = FALSE;
//}

//void Clear_Uart3Buff(void)
//{
//	memset(g_USART3_RX_BUF, 0, USART3_REC_LEN);
//	g_USART3_RX_CNT = 0;
//	g_Uart3RxFlag = FALSE;
//}

//void Clear_Uart4Buff(void)
//{
//	memset(g_USART4_RX_BUF, 0, USART4_REC_LEN);
//	g_USART4_RX_CNT = 0;
//	g_Uart4RxFlag = FALSE;
//}

//void Clear_Uart5Buff(void)
//{
//	memset(g_USART5_RX_BUF, 0, USART5_REC_LEN);
//	g_USART5_RX_CNT = 0;
//	g_Uart5RxFlag = FALSE;
//}
//初始化IO 串口1 
//bound:波特率
void Uart1_Init(u32 bound)
{
	
	NVIC_InitTypeDef   NVIC_InitStructure;	
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);	//使能USART1，GPIOA时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
	//USART1_TX   GPIOA.9
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //PA.9
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//复用推挽输出
	GPIO_Init(GPIOA, &GPIO_InitStructure);//初始化GPIOA.9

	//USART1_RX	  GPIOA.10初始化
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;//PA10
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//浮空输入
	GPIO_Init(GPIOA, &GPIO_InitStructure);//初始化GPIOA.10  

	USART_InitStructure.USART_BaudRate = bound;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);
	
		/* Enable the USARTx Interrupt */

	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);
	/* Enable USART */
	USART_Cmd(USART1, ENABLE);
	
	DMA_Uart1_Config();

}
//初始化IO 串口1 
////bound:波特率
//void Uart2_Init(u32 bound)
//{
//	
//	NVIC_InitTypeDef   NVIC_InitStructure;	
//	GPIO_InitTypeDef GPIO_InitStructure;
//	USART_InitTypeDef USART_InitStructure;
//	
//	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
//	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
//	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
//	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//	NVIC_Init(&NVIC_InitStructure);
//	
//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2 | RCC_APB2Periph_GPIOA, ENABLE);	//使能USART1，GPIOA时钟

//	//USART1_TX   GPIOA.9
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2; //PA.9
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//复用推挽输出
//	GPIO_Init(GPIOA, &GPIO_InitStructure);//初始化GPIOA.9

//	//USART1_RX	  GPIOA.10初始化
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;//PA10
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//浮空输入
//	GPIO_Init(GPIOA, &GPIO_InitStructure);//初始化GPIOA.10  

//	USART_InitStructure.USART_BaudRate = bound;
//	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
//	USART_InitStructure.USART_StopBits = USART_StopBits_1;
//	USART_InitStructure.USART_Parity = USART_Parity_No;
//	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
//	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
//	USART_Init(USART2, &USART_InitStructure);
//	
//		/* Enable the USARTx Interrupt */

////	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
//	USART_ITConfig(USART2, USART_IT_IDLE, ENABLE);
//	/* Enable USART */
//	USART_Cmd(USART2, ENABLE);
//	
//	DMA_Uart2_Config();

//}

//void Uart3_Init(u32 bound)
//{
//	
//	NVIC_InitTypeDef   NVIC_InitStructure;	
//	GPIO_InitTypeDef GPIO_InitStructure;
//	USART_InitTypeDef USART_InitStructure;
//	
//	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
//	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
//	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
//	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//	NVIC_Init(&NVIC_InitStructure);
//	
//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3 | RCC_APB2Periph_GPIOB, ENABLE);	//使能USART1，GPIOA时钟
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
//	//USART1_TX   GPIOA.9
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10; //PA.9
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//复用推挽输出
//	GPIO_Init(GPIOB, &GPIO_InitStructure);//初始化GPIOA.9

//	//USART1_RX	  GPIOA.10初始化
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;//PA10
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//浮空输入
//	GPIO_Init(GPIOB, &GPIO_InitStructure);//初始化GPIOA.10  

//	USART_InitStructure.USART_BaudRate = bound;
//	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
//	USART_InitStructure.USART_StopBits = USART_StopBits_1;
//	USART_InitStructure.USART_Parity = USART_Parity_No;
//	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
//	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
//	USART_Init(USART3, &USART_InitStructure);
//	
//		/* Enable the USARTx Interrupt */

////	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
//	USART_ITConfig(USART3, USART_IT_IDLE, ENABLE);
//	/* Enable USART */
//	USART_Cmd(USART3, ENABLE);
//	
//	DMA_Uart3_Config();

//}

void USART1_IRQHandler(void)                
{
	u8 Rec = 0;
	
	if(USART_GetITStatus(USART1, USART_IT_RXNE) == SET)
	{
		Rec = USART_ReceiveData(USART1); //Clear Flag
		if(g_USART_RX_CNT<USART_REC_LEN)
		{
			g_USART_RX_BUF[g_USART_RX_CNT] = Rec;
			g_USART_RX_CNT++;			 
		} 		
		else
		{
			Clear_Uart1Buff();
		}
		
		USART_GetFlagStatus(USART1,USART_FLAG_ORE);
		USART_ClearITPendingBit(USART1,USART_IT_RXNE);
	}
	else if(USART_GetITStatus(USART1, USART_IT_IDLE) == SET)
	{
		USART1->DR; //Clear Flag
		g_USART_RX_BUF[g_USART_RX_CNT] = '\0';		
		g_UartRxFlag = TRUE;
	}		
} 

//void USART2_IRQHandler(void)                
//{
//	u8 Rec = 0;
//	static u16 s_RecCount = 0;
//	
//	if(USART_GetITStatus(USART2, USART_IT_RXNE) == SET)
//	{
//		Rec = USART_ReceiveData(USART2); //Clear Flag
//		g_USART2_RX_BUF[s_RecCount++] = Rec;
//		if(s_RecCount >= USART2_REC_LEN - 1)
//		{
//			s_RecCount = 0;
//		}
//	}
//	else if(USART_GetITStatus(USART2, USART_IT_IDLE) == SET)
//	{
//		Rec = USART2->DR; //Clear Flag
//		g_USART2_RX_BUF[s_RecCount] = '\0';	
//		g_USART2_RX_CNT = s_RecCount;
//		s_RecCount = 0;
//		g_Uart2RxFlag = TRUE;
//	}		
//} 

//void USART3_IRQHandler(void)                
//{
//	u8 Rec = 0;
//	static u16 s_RecCount = 0;
//	
//	if(USART_GetITStatus(USART3, USART_IT_RXNE) == SET)
//	{
//		Rec = USART_ReceiveData(USART3); //Clear Flag
//		g_USART3_RX_BUF[s_RecCount++] = Rec;
//		if(s_RecCount >= USART3_REC_LEN - 1)
//		{
//			s_RecCount = 0;
//		}
//	}
//	else if(USART_GetITStatus(USART3, USART_IT_IDLE) == SET)
//	{
//		Rec = USART3->DR; //Clear Flag
//		g_USART3_RX_BUF[s_RecCount] = '\0';	
//		g_USART3_RX_CNT = s_RecCount;
//		s_RecCount = 0;
//		g_Uart3RxFlag = TRUE;
//	}		
//} 
//void USART1_IRQHandler(void)                
//{
//	if(USART_GetITStatus(USART1,USART_IT_IDLE) == SET)
//	{
//		USART1->SR;
//		USART1->DR; //Clear Flag
//		DMA_Cmd(DMA1_Channel5,DISABLE);
//		
//		g_USART_RX_CNT = USART_REC_LEN - DMA_GetCurrDataCounter(DMA1_Channel5);
//		if(g_USART_RX_CNT < USART_REC_LEN)
//		{
//			g_USART_RX_BUF[g_USART_RX_CNT] = '\0';
//		}
//		else
//		{
//			g_USART_RX_BUF[g_USART_RX_CNT-1] = '\0';	
//		}		
//		g_UartRxFlag = TRUE;
//		DMA_SetCurrDataCounter(DMA1_Channel5,USART_REC_LEN);
//		DMA_Cmd(DMA1_Channel5,ENABLE);  //??DMA??
//	}		
//} 

//void USART2_IRQHandler(void)            
//{
//	if(USART_GetITStatus(USART2,USART_IT_IDLE) == SET)
//	{
//		USART2->SR;
//		USART2->DR; //Clear Flag
//		DMA_Cmd(DMA1_Channel6,DISABLE);
//		
//		g_USART2_RX_CNT = USART2_REC_LEN - DMA_GetCurrDataCounter(DMA1_Channel6);
//		if(g_USART2_RX_CNT < USART2_REC_LEN)
//		{
//			g_USART2_RX_BUF[g_USART2_RX_CNT] = '\0';
//		}
//		else
//		{
//			g_USART2_RX_BUF[g_USART2_RX_CNT-1] = '\0';	
//		}	
//		g_Uart2RxFlag = TRUE;
//		DMA_SetCurrDataCounter(DMA1_Channel6,USART2_REC_LEN);
//		DMA_Cmd(DMA1_Channel6,ENABLE);  //??DMA??
//	}	
//} 


//void USART3_IRQHandler(void)            
//{
//	if(USART_GetITStatus(USART3,USART_IT_IDLE) == SET)
//	{
//		USART3->SR;
//		USART3->DR; //Clear Flag
//		DMA_Cmd(DMA1_Channel3,DISABLE);
//		
//		g_USART3_RX_CNT = USART3_REC_LEN - DMA_GetCurrDataCounter(DMA1_Channel3);
//		if(g_USART3_RX_CNT < USART3_REC_LEN)
//		{
//			g_USART3_RX_BUF[g_USART3_RX_CNT] = '\0';
//		}
//		else
//		{
//			g_USART3_RX_BUF[g_USART3_RX_CNT-1] = '\0';	
//		}		
//		g_Uart3RxFlag = TRUE;
//		DMA_SetCurrDataCounter(DMA1_Channel3,USART3_REC_LEN);
//		DMA_Cmd(DMA1_Channel3,ENABLE);  //??DMA??
//	}	
//} 




