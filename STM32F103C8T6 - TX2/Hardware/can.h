#ifndef __CAN_H
#define __CAN_H	 
#include "sys.h"	    


//CAN1接收RX0中断使能
#define CAN1_RX0_INT_ENABLE	1		//0,不使能;1,使能.								    
										 							 				    
u8 CAN_Mode_Init(u16 brp, u8 mode);//CAN1初始化

u8 Can_Send_Msg(unsigned int StdID, u8* msg,u8 len);

unsigned int Can_Receive_Msg(u8 *buf);

#endif

















