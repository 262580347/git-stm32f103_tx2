#include "sys.h"
#include "DMA.h"

__align(8) u8 USART1_TX_BUF[500]; 	      //发送缓冲,最大256字节
//__align(8) u8 USART2_TX_BUF[500]; 	      //发送缓冲,最大100
//__align(8) u8 USART3_TX_BUF[500]; 	      //发送缓冲,最大256字节
//__align(8) u8 USART4_TX_BUF[500]; 	      //发送缓冲,最大100
//__align(8) u8 USART5_TX_BUF[500]; 	

void DMA_Uart1_Config(void)
{
	DMA_InitTypeDef           DMA_InitStructure;
	/* Enable DMA1 clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	
    DMA_InitStructure.DMA_PeripheralBaseAddr  = (uint32_t)(&(USART1->DR));   /* 目的 */
    DMA_InitStructure.DMA_MemoryBaseAddr     = (uint32_t)USART1_TX_BUF;             /* 源 */
    DMA_InitStructure.DMA_DIR                 = DMA_DIR_PeripheralDST;    /* 方向 */
    DMA_InitStructure.DMA_BufferSize          = 0;                    /* 长度 */                  
    DMA_InitStructure.DMA_PeripheralInc       = DMA_PeripheralInc_Disable;    /* 外设地址是否自增 */
    DMA_InitStructure.DMA_MemoryInc           = DMA_MemoryInc_Enable;         /* 内存地址是否自增 */
    DMA_InitStructure.DMA_PeripheralDataSize  = DMA_MemoryDataSize_Byte;      /* 目的数据带宽 */
    DMA_InitStructure.DMA_MemoryDataSize      = DMA_MemoryDataSize_Byte;      /* 源数据宽度 */
    DMA_InitStructure.DMA_Mode                = DMA_Mode_Normal;              /* 单次传输模式/循环传输模式 */
    DMA_InitStructure.DMA_Priority            = DMA_Priority_High;             /* DMA优先级 */
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;  //DMA通道x没有设置为内存到内存传输

    /* 3. 配置DMA */
    DMA_Init(DMA1_Channel4, &DMA_InitStructure); 

	USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE); 
	
	/* DMA1 Channel5 (triggered by USART1 Rx event) Config */	
//	DMA_DeInit(DMA1_Channel5);
//	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&(USART1->DR));// 初始化外设地址，相当于“哪家快递”  
//	DMA_InitStructure.DMA_MemoryBaseAddr =(uint32_t)&g_USART_RX_BUF;// 内存地址，相当于几号柜
//	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;//外设作为数据来源，即为收快递
//	DMA_InitStructure.DMA_BufferSize = USART_REC_LEN ;// 缓存容量，即柜子大小
//	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; // 外设地址不递增，即柜子对应的快递不变
//	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;// 内存递增
//	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte; //外设字节宽度，即快递运输快件大小度量（按重量算，还是按体积算） 
//	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;// 内存字节宽度，即店主封装快递的度量(按重量，还是按体质进行封装)
//	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;// 正常模式，即满了就不在接收了，而不是循环存储
//	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;// 优先级很高，对应快递就是加急
//	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable; // 内存与外设通信，而非内存到内存 
//	DMA_Init(DMA1_Channel5, &DMA_InitStructure);// 把参数初始化，即拟好与快递公司的协议

//	DMA_Cmd(DMA1_Channel5, ENABLE);// 启动DMA，即与快递公司签订合同，正式生效
//	USART_DMACmd(USART1,USART_DMAReq_Rx,ENABLE); 
}

//void DMA_Uart2_Config(void)
//{
//	DMA_InitTypeDef           DMA_InitStructure;
//	/* Enable DMA1 clock */
//	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
//	
//    DMA_InitStructure.DMA_PeripheralBaseAddr  = (uint32_t)(&(USART2->DR));   /* 目的 */
//    DMA_InitStructure.DMA_MemoryBaseAddr     = (uint32_t)USART2_TX_BUF;             /* 源 */
//    DMA_InitStructure.DMA_DIR                 = DMA_DIR_PeripheralDST;    /* 方向 */
//    DMA_InitStructure.DMA_BufferSize          = 0;                    /* 长度 */                  
//    DMA_InitStructure.DMA_PeripheralInc       = DMA_PeripheralInc_Disable;    /* 外设地址是否自增 */
//    DMA_InitStructure.DMA_MemoryInc           = DMA_MemoryInc_Enable;         /* 内存地址是否自增 */
//    DMA_InitStructure.DMA_PeripheralDataSize  = DMA_MemoryDataSize_Byte;      /* 目的数据带宽 */
//    DMA_InitStructure.DMA_MemoryDataSize      = DMA_MemoryDataSize_Byte;      /* 源数据宽度 */
//    DMA_InitStructure.DMA_Mode                = DMA_Mode_Normal;              /* 单次传输模式/循环传输模式 */
//    DMA_InitStructure.DMA_Priority            = DMA_Priority_High;             /* DMA优先级 */
//    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;  //DMA通道x没有设置为内存到内存传输

//    /* 3. 配置DMA */
//    DMA_Init(DMA1_Channel7, &DMA_InitStructure); 

//	USART_DMACmd(USART2, USART_DMAReq_Tx, ENABLE); 
//	
//	/* DMA1 Channel5 (triggered by USART1 Rx event) Config */
//	DMA_DeInit(DMA1_Channel6);
//	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&(USART2->DR));// 初始化外设地址，相当于“哪家快递”  
//	DMA_InitStructure.DMA_MemoryBaseAddr =(uint32_t)&g_USART2_RX_BUF;// 内存地址，相当于几号柜
//	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;//外设作为数据来源，即为收快递
//	DMA_InitStructure.DMA_BufferSize = USART2_REC_LEN ;// 缓存容量，即柜子大小
//	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; // 外设地址不递增，即柜子对应的快递不变
//	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;// 内存递增
//	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte; //外设字节宽度，即快递运输快件大小度量（按重量算，还是按体积算） 
//	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;// 内存字节宽度，即店主封装快递的度量(按重量，还是按体质进行封装)
//	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;// 正常模式，即满了就不在接收了，而不是循环存储
//	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;// 优先级很高，对应快递就是加急
//	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable; // 内存与外设通信，而非内存到内存 
//	DMA_Init(DMA1_Channel6, &DMA_InitStructure);// 把参数初始化，即拟好与快递公司的协议

//	DMA_Cmd(DMA1_Channel6, ENABLE);// 启动DMA，即与快递公司签订合同，正式生效
//	USART_DMACmd(USART2,USART_DMAReq_Rx,ENABLE); 
//}

//void DMA_Uart3_Config(void)
//{
//	DMA_InitTypeDef           DMA_InitStructure;
//	/* Enable DMA1 clock */
//	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
//	
//    DMA_InitStructure.DMA_PeripheralBaseAddr  = (uint32_t)(&(USART3->DR));   /* 目的 */
//    DMA_InitStructure.DMA_MemoryBaseAddr     = (uint32_t)USART3_TX_BUF;             /* 源 */
//    DMA_InitStructure.DMA_DIR                 = DMA_DIR_PeripheralDST;    /* 方向 */
//    DMA_InitStructure.DMA_BufferSize          = 0;                    /* 长度 */                  
//    DMA_InitStructure.DMA_PeripheralInc       = DMA_PeripheralInc_Disable;    /* 外设地址是否自增 */
//    DMA_InitStructure.DMA_MemoryInc           = DMA_MemoryInc_Enable;         /* 内存地址是否自增 */
//    DMA_InitStructure.DMA_PeripheralDataSize  = DMA_MemoryDataSize_Byte;      /* 目的数据带宽 */
//    DMA_InitStructure.DMA_MemoryDataSize      = DMA_MemoryDataSize_Byte;      /* 源数据宽度 */
//    DMA_InitStructure.DMA_Mode                = DMA_Mode_Normal;              /* 单次传输模式/循环传输模式 */
//    DMA_InitStructure.DMA_Priority            = DMA_Priority_High;             /* DMA优先级 */
//    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;  //DMA通道x没有设置为内存到内存传输

//    /* 3. 配置DMA */
//    DMA_Init(DMA1_Channel2, &DMA_InitStructure); 

//	USART_DMACmd(USART3, USART_DMAReq_Tx, ENABLE); 
//	
//	/* DMA1 Channel5 (triggered by USART1 Rx event) Config */
//	DMA_DeInit(DMA1_Channel3);
//	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&(USART3->DR));// 初始化外设地址，相当于“哪家快递”  
//	DMA_InitStructure.DMA_MemoryBaseAddr =(uint32_t)&g_USART3_RX_BUF;// 内存地址，相当于几号柜
//	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;//外设作为数据来源，即为收快递
//	DMA_InitStructure.DMA_BufferSize = USART3_REC_LEN ;// 缓存容量，即柜子大小
//	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; // 外设地址不递增，即柜子对应的快递不变
//	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;// 内存递增
//	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte; //外设字节宽度，即快递运输快件大小度量（按重量算，还是按体积算） 
//	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;// 内存字节宽度，即店主封装快递的度量(按重量，还是按体质进行封装)
//	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;// 正常模式，即满了就不在接收了，而不是循环存储
//	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;// 优先级很高，对应快递就是加急
//	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable; // 内存与外设通信，而非内存到内存 
//	DMA_Init(DMA1_Channel3, &DMA_InitStructure);// 把参数初始化，即拟好与快递公司的协议

//	DMA_Cmd(DMA1_Channel3, ENABLE);// 启动DMA，即与快递公司签订合同，正式生效
//	USART_DMACmd(USART3,USART_DMAReq_Rx,ENABLE); 
//}

//void DMA_Uart4_Config(void)
//{
//	DMA_InitTypeDef           DMA_InitStructure;
//	/* Enable DMA1 clock */
//	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA2, ENABLE);
//	
//    DMA_InitStructure.DMA_PeripheralBaseAddr  = (uint32_t)(&(UART4->DR));   /* 目的 */
//    DMA_InitStructure.DMA_MemoryBaseAddr     = (uint32_t)USART4_TX_BUF;             /* 源 */
//    DMA_InitStructure.DMA_DIR                 = DMA_DIR_PeripheralDST;    /* 方向 */
//    DMA_InitStructure.DMA_BufferSize          = 0;                    /* 长度 */                  
//    DMA_InitStructure.DMA_PeripheralInc       = DMA_PeripheralInc_Disable;    /* 外设地址是否自增 */
//    DMA_InitStructure.DMA_MemoryInc           = DMA_MemoryInc_Enable;         /* 内存地址是否自增 */
//    DMA_InitStructure.DMA_PeripheralDataSize  = DMA_MemoryDataSize_Byte;      /* 目的数据带宽 */
//    DMA_InitStructure.DMA_MemoryDataSize      = DMA_MemoryDataSize_Byte;      /* 源数据宽度 */
//    DMA_InitStructure.DMA_Mode                = DMA_Mode_Normal;              /* 单次传输模式/循环传输模式 */
//    DMA_InitStructure.DMA_Priority            = DMA_Priority_High;             /* DMA优先级 */
//    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;  //DMA通道x没有设置为内存到内存传输

//    /* 3. 配置DMA */
//    DMA_Init(DMA2_Channel5, &DMA_InitStructure); 

//	USART_DMACmd(UART4, USART_DMAReq_Tx, ENABLE); 
//	
//	/* DMA1 Channel5 (triggered by USART1 Rx event) Config */
//	DMA_DeInit(DMA2_Channel3);
//	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&(UART4->DR));// 初始化外设地址，相当于“哪家快递”  
//	DMA_InitStructure.DMA_MemoryBaseAddr =(uint32_t)&g_USART4_RX_BUF;// 内存地址，相当于几号柜
//	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;//外设作为数据来源，即为收快递
//	DMA_InitStructure.DMA_BufferSize = USART4_REC_LEN ;// 缓存容量，即柜子大小
//	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; // 外设地址不递增，即柜子对应的快递不变
//	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;// 内存递增
//	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte; //外设字节宽度，即快递运输快件大小度量（按重量算，还是按体积算） 
//	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;// 内存字节宽度，即店主封装快递的度量(按重量，还是按体质进行封装)
//	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;// 正常模式，即满了就不在接收了，而不是循环存储
//	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;// 优先级很高，对应快递就是加急
//	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable; // 内存与外设通信，而非内存到内存 
//	DMA_Init(DMA2_Channel3, &DMA_InitStructure);// 把参数初始化，即拟好与快递公司的协议

//	DMA_Cmd(DMA2_Channel3, ENABLE);// 启动DMA，即与快递公司签订合同，正式生效
//	USART_DMACmd(UART4,USART_DMAReq_Rx,ENABLE); 
//}


void u1_printf(char* fmt,...)  
{  
	int i;
	va_list ap;
	
	while (DMA_GetCurrDataCounter(DMA1_Channel4));
	if(DMA_GetFlagStatus(DMA1_FLAG_TC4) == SET)
		DMA_ClearFlag(DMA1_FLAG_TC4);
	DMA_Cmd(DMA1_Channel4, DISABLE );  //关闭USART1 TX DMA1 所指示的通道      
	memset(USART1_TX_BUF, 0, sizeof(USART1_TX_BUF));
	va_start(ap,fmt);
	vsprintf((char*)USART1_TX_BUF,fmt,ap);
	va_end(ap);
	i=strlen((const char*)USART1_TX_BUF);//此次发送数据的长度	
 
	DMA_SetCurrDataCounter(DMA1_Channel4,i);//DMA通道的DMA缓存的大小
	
	DMA_Cmd(DMA1_Channel4, ENABLE);  //使能USART1 TX DMA1 所指示的通道 
}

void USART1_DMA_Send(unsigned char *buffer, unsigned short buffer_size)
{
	while (DMA_GetCurrDataCounter(DMA1_Channel4));
	if(DMA_GetFlagStatus(DMA1_FLAG_TC4) == SET)
		DMA_ClearFlag(DMA1_FLAG_TC4);
	DMA_Cmd(DMA1_Channel4, DISABLE );  //关闭USART1 TX DMA1 所指示的通道      

	memcpy(&USART1_TX_BUF[0], buffer, buffer_size);
 
	DMA_SetCurrDataCounter(DMA1_Channel4,buffer_size);
	DMA_Cmd(DMA1_Channel4, ENABLE);
}

//void u2_printf(char* fmt,...)  
//{  
//	int i;
//	va_list ap;
//	
//	while (DMA_GetCurrDataCounter(DMA1_Channel7));
//	if(DMA_GetFlagStatus(DMA1_FLAG_TC7) == SET)
//		DMA_ClearFlag(DMA1_FLAG_TC7);
//	DMA_Cmd(DMA1_Channel7, DISABLE );  //关闭USART1 TX DMA1 所指示的通道      
//	memset(USART2_TX_BUF, 0, sizeof(USART2_TX_BUF));
//	va_start(ap,fmt);
//	vsprintf((char*)USART2_TX_BUF,fmt,ap);
//	va_end(ap);
//	i=strlen((const char*)USART2_TX_BUF);//此次发送数据的长度	
// 
//	DMA_SetCurrDataCounter(DMA1_Channel7,i);//DMA通道的DMA缓存的大小
//	
//	DMA_Cmd(DMA1_Channel7, ENABLE);  //使能USART1 TX DMA1 所指示的通道 
//}

//void USART2_DMA_Send(unsigned char *buffer, unsigned short buffer_size)
//{
//	while (DMA_GetCurrDataCounter(DMA1_Channel7));
//	if(DMA_GetFlagStatus(DMA1_FLAG_TC7) == SET)
//		DMA_ClearFlag(DMA1_FLAG_TC7);
//	DMA_Cmd(DMA1_Channel7, DISABLE );  //关闭USART1 TX DMA1 所指示的通道      

//	memcpy(&USART2_TX_BUF[0], buffer, buffer_size);
// 
//	DMA_SetCurrDataCounter(DMA1_Channel7,buffer_size);
//	DMA_Cmd(DMA1_Channel7, ENABLE);
//}

//void u3_printf(char* fmt,...)  
//{  
//	int i;
//	va_list ap;
//	
//	while (DMA_GetCurrDataCounter(DMA1_Channel2));
//	if(DMA_GetFlagStatus(DMA1_FLAG_TC2) == SET)
//		DMA_ClearFlag(DMA1_FLAG_TC2);
//	DMA_Cmd(DMA1_Channel2, DISABLE );  //关闭USART1 TX DMA1 所指示的通道      
//	memset(USART3_TX_BUF, 0, sizeof(USART3_TX_BUF));
//	va_start(ap,fmt);
//	vsprintf((char*)USART3_TX_BUF,fmt,ap);
//	va_end(ap);
//	i=strlen((const char*)USART3_TX_BUF);//此次发送数据的长度	
// 
//	DMA_SetCurrDataCounter(DMA1_Channel2,i);//DMA通道的DMA缓存的大小
//	
//	DMA_Cmd(DMA1_Channel2, ENABLE);  //使能USART1 TX DMA1 所指示的通道 
//}

//void USART3_DMA_Send(unsigned char *buffer, unsigned short buffer_size)
//{
//	while (DMA_GetCurrDataCounter(DMA1_Channel2));
//	if(DMA_GetFlagStatus(DMA1_FLAG_TC2) == SET)
//		DMA_ClearFlag(DMA1_FLAG_TC2);
//	DMA_Cmd(DMA1_Channel2, DISABLE );  //关闭USART1 TX DMA1 所指示的通道      

//	memcpy(&USART3_TX_BUF[0], buffer, buffer_size);
// 
//	DMA_SetCurrDataCounter(DMA1_Channel2,buffer_size);
//	DMA_Cmd(DMA1_Channel2, ENABLE);
//}

//void u4_printf(char* fmt,...)  
//{  
//	int i;
//	va_list ap;
//	
//	while (DMA_GetCurrDataCounter(DMA2_Channel5));
//	if(DMA_GetFlagStatus(DMA2_FLAG_TC5) == SET)
//		DMA_ClearFlag(DMA2_FLAG_TC5);
//	DMA_Cmd(DMA2_Channel5, DISABLE );  //关闭USART1 TX DMA1 所指示的通道      
//	memset(USART4_TX_BUF, 0, sizeof(USART4_TX_BUF));
//	va_start(ap,fmt);
//	vsprintf((char*)USART4_TX_BUF,fmt,ap);
//	va_end(ap);
//	i=strlen((const char*)USART4_TX_BUF);//此次发送数据的长度	
// 
//	DMA_SetCurrDataCounter(DMA2_Channel5,i);//DMA通道的DMA缓存的大小
//	
//	DMA_Cmd(DMA2_Channel5, ENABLE);  //使能USART1 TX DMA1 所指示的通道 
//}

//void UART4_DMA_Send(unsigned char *buffer, unsigned short buffer_size)
//{
//	while (DMA_GetCurrDataCounter(DMA2_Channel5));
//	if(DMA_GetFlagStatus(DMA2_FLAG_TC5) == SET)
//		DMA_ClearFlag(DMA2_FLAG_TC5);
//	DMA_Cmd(DMA2_Channel5, DISABLE );  //关闭USART1 TX DMA1 所指示的通道      

//	memcpy(&USART4_TX_BUF[0], buffer, buffer_size);
// 
//	DMA_SetCurrDataCounter(DMA2_Channel5,buffer_size);
//	DMA_Cmd(DMA2_Channel5, ENABLE);
//}
