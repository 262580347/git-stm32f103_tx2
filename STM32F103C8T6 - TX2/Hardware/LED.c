#include "led.h"
  /**************************************************************************
作者：平衡小车之家
我的淘宝小店：http://shop114407458.taobao.com/
**************************************************************************/
/**************************************************************************
函数功能：LED闪烁
入口参数：闪烁频率 
返回  值：无
**************************************************************************/
void TaskForLED1Blink(void)
{
	  LED1_OUT() = ~LED1_OUT();
}
void TaskForLED2Blink(void)
{
	static unsigned char s_Status = 1;
	
	
	if(GetCanRecFlag() == FALSE && GetCanSendFlag() == FALSE)
	{
		LED2_OUT() = ~LED2_OUT();
	}
	else if(GetCanRecFlag() == TRUE && GetCanSendFlag() == TRUE)
	{
		LED2_OUT() = 1;
	}
	else
	{
		LED2_OUT() = 0;
	}
}
/**************************************************************************
函数功能：LED接口初始化
入口参数：无 
返回  值：无
**************************************************************************/
void LED_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD, ENABLE); //使能端口时钟
	
	
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;	            //端口配置
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;      //推挽输出
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //50M
//	GPIO_Init(GPIOA, &GPIO_InitStructure);					      //根据设定参数初始化GPIO
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;	            //端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;      //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //50M
	GPIO_Init(GPIOB, &GPIO_InitStructure);					      //根据设定参数初始化GPIO
	
	GPIO_InitStructure.GPIO_Pin = LED1_GPIO_PIN;	            //端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;      //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //50M
	GPIO_Init(LED1_GPIO_TYPE, &GPIO_InitStructure);					      //根据设定参数初始化GPIO
	
	GPIO_InitStructure.GPIO_Pin = LED2_GPIO_PIN;	            //端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;      //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //50M
	GPIO_Init(LED2_GPIO_TYPE, &GPIO_InitStructure);					      //根据设定参数初始化GPIO
	
	Task_Create(TaskForLED1Blink, 50000);
	
	Task_Create(TaskForLED2Blink, 30000);
	

}

