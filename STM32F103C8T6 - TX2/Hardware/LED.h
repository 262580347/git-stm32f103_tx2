#ifndef __LED_H
#define __LED_H
#include "sys.h"
  /**************************************************************************
作者：平衡小车之家
我的淘宝小店：http://shop114407458.taobao.com/
**************************************************************************/
//LED 端口定义
#define		LED1_GPIO_TYPE	GPIOB
#define		LED2_GPIO_TYPE	GPIOA

#define		LED1_GPIO_PIN	GPIO_Pin_3
#define		LED2_GPIO_PIN	GPIO_Pin_15

#define		BEEP_GPIO_TYPE	GPIOA
#define		BEEP_GPIO_PIN	GPIO_Pin_4


#define		LED1_OUT()	PBout(3)
#define		LED2_OUT()	PAout(15)

#define		BEEP_OUT() 	PAout(4)



void LED_Init(void);

#endif
