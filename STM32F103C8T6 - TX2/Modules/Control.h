#ifndef		_CONTROL_H_
#define		_CONTROL_H_


void CarControlInit(void);

void Get_PS2_Data(void);

int GetLeftMotorOut(void);

int GetRightMotorOut(void);

void ControlStop(void);

void SetMotorOut(int MotorTargetLeft , int MotorTargetRight);
	
#endif


