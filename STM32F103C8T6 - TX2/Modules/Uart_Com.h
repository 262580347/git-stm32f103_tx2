#ifndef  _UART_COM_
#define	 _UART_COM_


void TaskForDebugCOM(void);

void TaskForUart5Communication(void);

void TaskForCOM2(void);

void TaskForCOM3(void);

void TaskForCOM4(void);

void TaskForCOM5(void);

unsigned char GetShowDataFlag(void);

int GetRange2(void);

int GetRange3(void);

int GetRange4(void);

unsigned char GetBlutoothDebugFlag(void);

#endif

