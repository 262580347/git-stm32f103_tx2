
#include "sys.h"
#include "Control.h"

#define		DEADZONE		3
#define		BUF_SIZE		5
#define		MAX_SPEED_VALUE		1500

#define		CAR_CAN_ID		0x601

int s_MotorTargetLeft = 0, s_MotorTargetRight = 0;

int GetLeftMotorOut(void)
{
	return s_MotorTargetLeft;
}

int GetRightMotorOut(void)
{
	return s_MotorTargetRight;
}

void SetMotorOut(int MotorTargetLeft , int MotorTargetRight)
{
	s_MotorTargetLeft = MotorTargetLeft;
	s_MotorTargetRight = MotorTargetRight;
}



//void ControlStop(void)
//{
//	unsigned char CanBuf[8];
//	
//	CanBuf[0]=0X23;	//写命令
//	CanBuf[1]=0X00;	//索引低位
//	CanBuf[2]=0X20;	//索引高位
//	CanBuf[3]=0X01;	//子索引
//	CanBuf[4]=0;
//	CanBuf[5]=0;
//	CanBuf[6]=0;
//	CanBuf[7]=0;
//	Can_Send_Msg(CAR_CAN_ID, CanBuf, 8);
//	
//	CanBuf[0]=0X23;	//写命令
//	CanBuf[1]=0X00;	//索引低位
//	CanBuf[2]=0X20;	//索引高位
//	CanBuf[3]=0X02;	//子索引
//	CanBuf[4]=0;
//	CanBuf[5]=0;
//	CanBuf[6]=0;
//	CanBuf[7]=0;
//	Can_Send_Msg(CAR_CAN_ID, CanBuf, 8);
//}

//松灵校验

static uint8 Agilex_CANMsgChecksum(uint16 id, uint8 *data, uint8 len)
{
	uint8 checksum = 0x00, i;
	
	checksum = (uint8)(id&0x00ff) + (uint8)(id>>8) + len;
	
	for(i=0; i<(len-1); i++)
	{
		checksum += data[i];
	}
	
	return checksum;
}

void ControlStop(void)
{
	unsigned char CanBuf[8], i = 0;
	static unsigned char s_Count = 0;
	
	CanBuf[0]=0X01;	//CAN指令模式
	CanBuf[1]=0X00;	//故障清除指令（不清除）
	CanBuf[2]=0X00;	//线速度百分比
	CanBuf[3]=0X00;	//角速度百分比
	CanBuf[4]=0;	//保留
	CanBuf[5]=0;	//保留
	CanBuf[6]=s_Count++;	//计数校验
	CanBuf[7]=Agilex_CANMsgChecksum(MCU1_ID, CanBuf, 8);
	
//	for(i=0; i<8; i++)
//	{
//		u1_printf("%02X ", CanBuf[i]);
//	}
//	u1_printf("\r\n");
	
	Can_Send_Msg(CAR_CAN_ID, CanBuf, 8);
	
}

static void TaskForCarControl(void)
{
	static unsigned char s_isFirst = FALSE;

	if(s_isFirst == FALSE)
	{
		ControlStop();
		s_isFirst = TRUE;
	}
}

void CarControlInit(void)
{
	Task_Create(TaskForCarControl, 10000);
	u1_printf(" Car Control Init...\r\n");
}

