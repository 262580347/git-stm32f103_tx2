#ifndef _POWERCONTROL_H_

#define	_POWERCONTROL_H_

#define		POWER1_CONTROL_OUT()		PBout(4)
#define		POWER2_CONTROL_OUT()		PBout(5)
#define		POWER3_CONTROL_OUT()		PBout(6)
#define		POWER4_CONTROL_OUT()		PBout(7)
#define		POWER5_CONTROL_OUT()		PBout(8)
#define		POWER6_CONTROL_OUT()		PBout(9)

#define		LED_POWER_EN_GPIO_TYPE		GPIOA
#define		LED_POWER_EN_GPIO_PIN		GPIO_Pin_5
#define		LED_EN_OUT() 				PAout(5)

#define		POWER6_EN_GPIO_TYPE		GPIOB
#define		POWER6_EN_GPIO_PIN		GPIO_Pin_4
#define		POWER5_EN_GPIO_TYPE		GPIOB
#define		POWER5_EN_GPIO_PIN		GPIO_Pin_5
#define		POWER4_EN_GPIO_TYPE		GPIOB
#define		POWER4_EN_GPIO_PIN		GPIO_Pin_6
#define		POWER3_EN_GPIO_TYPE		GPIOB
#define		POWER3_EN_GPIO_PIN		GPIO_Pin_7
#define		POWER2_EN_GPIO_TYPE		GPIOB
#define		POWER2_EN_GPIO_PIN		GPIO_Pin_8
#define		POWER1_EN_GPIO_TYPE		GPIOB
#define		POWER1_EN_GPIO_PIN		GPIO_Pin_9

#define		POWER1_CONTROL_ON()			POWER1_CONTROL_OUT() = 1
#define		POWER1_CONTROL_OFF()		POWER1_CONTROL_OUT() = 0
#define		POWER2_CONTROL_ON()			POWER2_CONTROL_OUT() = 1
#define		POWER2_CONTROL_OFF()		POWER2_CONTROL_OUT() = 0
#define		POWER3_CONTROL_ON()			POWER3_CONTROL_OUT() = 1
#define		POWER3_CONTROL_OFF()		POWER3_CONTROL_OUT() = 0
#define		POWER4_CONTROL_ON()			POWER4_CONTROL_OUT() = 1
#define		POWER4_CONTROL_OFF()		POWER4_CONTROL_OUT() = 0
#define		POWER5_CONTROL_ON()			POWER5_CONTROL_OUT() = 1
#define		POWER5_CONTROL_OFF()		POWER5_CONTROL_OUT() = 0
#define		POWER6_CONTROL_ON()			POWER6_CONTROL_OUT() = 1
#define		POWER6_CONTROL_OFF()		POWER6_CONTROL_OUT() = 0

#define		POWER_CONTROL_NUM_1		1
#define		POWER_CONTROL_NUM_2		2

#define		POWER_ON				1
#define		POWER_OFF				0

#define		PWRKEY_GPIO_TYPE	GPIOA
#define		PWRKEY_GPIO_PIN	GPIO_Pin_2

#define		PWRKEY_OUT() 		PAout(2)

typedef struct
{
	unsigned char PowerStatus1;
	unsigned char PowerStatus2;
	unsigned char PowerStatus3;
	unsigned char PowerStatus4;
	unsigned char PowerStatus5;
	unsigned char PowerStatus6;
}POWERSTATUS;

POWERSTATUS* GetPowerStatus(void);

void PowerControlPortInit(void);

void CanControlPower(unsigned char Power_Num, unsigned char isOn);

void TX2PowerOn(void);

void TX2PowerOff(void);

void SystemPowerOff(void);

#endif
