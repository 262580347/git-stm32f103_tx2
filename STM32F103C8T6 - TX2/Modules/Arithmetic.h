#ifndef		_ARITHMETIC_H_
#define		_ARITHMETIC_H_

#define KALMAN_Q 0.02

#define KALMAN_R 7.0000

void SetKalmanFilterFlag(unsigned char isTrue);
unsigned char GetKalmanFilterFlag(void);
double KalmanFilter(const double ResrcData,double ProcessNiose_Q,double MeasureNoise_R);
double KalmanFilter2(const double ResrcData,double ProcessNiose_Q,double MeasureNoise_R);
double KalmanFilter3(const double ResrcData,double ProcessNiose_Q,double MeasureNoise_R);

unsigned short PackMsg(unsigned char* srcbuf,unsigned short srclen,unsigned char* destbuf,unsigned short destsize);

void Int32ToArray( unsigned char *pDst, unsigned int value );

float encode_float(float value);

float Mid_Filter(float * filter_data, unsigned short len) ;

int Mid_Filter_int(int * filter_data, unsigned short len) ;

float GetAverageValue(float * data, unsigned char len) ;

unsigned int ArrayToInt32(unsigned char *pDst, unsigned char count);

int toupper(int c);

void HexStrToByte(const char* source, unsigned char* dest, int sourceLen);

void StrToHex(unsigned char *pbDest, unsigned char *pbSrc, int nLen);

unsigned short Calculate_CRC16(unsigned char *buf,unsigned short length);

unsigned char Calc_Checksum(unsigned char *buf, unsigned int len);

unsigned int swap_dword(unsigned int value);
	
unsigned short int swap_word(unsigned short int value);

void Float2Byte(float *target,unsigned char *buf,unsigned char beg);







#endif



