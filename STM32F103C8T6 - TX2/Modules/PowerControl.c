#include "PowerControl.h"
#include "sys.h"

static int s_PowerOnTimer = 0, s_TX2OnTimer = 0;
static unsigned int s_PowerOffCount = 0;//关机计数
static POWERSTATUS s_PowerStatus;

POWERSTATUS* GetPowerStatus(void)
{
	return &s_PowerStatus;
}

void TX2PowerOn(void)	//控制引脚拉低1s，TX2开机
{
	if(GetTX2Status() != SYS_TX2_POWER_ON)
	{
		TX2_CTR_OUT() = 1;
		delay_ms(500);
		delay_ms(500);
		TX2_CTR_OUT() = 0;
		u1_printf("TX2 开机\r\n");
	}
	else
	{
		TX2_CTR_OUT() = 1;
		delay_ms(500);
		delay_ms(500);
		TX2_CTR_OUT() = 0;
		u1_printf("TX2 已开机\r\n");
	}

}

void TaskForTX2PowerOff(void)
{
	s_PowerOffCount++;
	
	BEEP_OUT() = 1;
	
	delay_ms(50);
	
	BEEP_OUT() = 0;
	
	if(s_PowerOffCount >= 10)	//10s 后
	{
		s_PowerOffCount = 0;
		KillTimer(s_TX2OnTimer);
		TX2_CTR_OUT() = 0;
		if(GetTX2Status() == SYS_TX2_POWER_OFF)
		{
			SystemPowerOff();
		}
	}
}
void TX2PowerOff(void)	//控制引脚拉低10s，TX2关机
{
	s_TX2OnTimer = SetTimer(10000, TaskForTX2PowerOff);
	s_PowerOffCount = 0;
	TX2_CTR_OUT() = 1;
}

void SystemPowerOff(void)
{
	u1_printf("1s后关闭电源\r\n");
	BeepRangThreeTimes();
	delay_ms(500);
	delay_ms(500);
	MCU_KEY_OUT() = 0;
}

void TaskForPowerControl(void)
{
	if(s_PowerStatus.PowerStatus1)
	{
		POWER1_CONTROL_ON();
	}
	else
	{
		POWER1_CONTROL_OFF();
	}
	
	if(s_PowerStatus.PowerStatus2)
	{
		POWER2_CONTROL_ON();
	}
	else
	{
		POWER2_CONTROL_OFF();
	}
	
	if(s_PowerStatus.PowerStatus3)
	{
		POWER3_CONTROL_ON();
	}
	else
	{
		POWER3_CONTROL_OFF();
	}
	
	if(s_PowerStatus.PowerStatus4)
	{
		POWER4_CONTROL_ON();
	}
	else
	{
		POWER4_CONTROL_OFF();
	}
	
	if(s_PowerStatus.PowerStatus5)
	{
		POWER5_CONTROL_ON();
	}
	else
	{
		POWER5_CONTROL_OFF();
	}
	
	if(s_PowerStatus.PowerStatus6)
	{
		POWER6_CONTROL_ON();
	}
	else
	{
		POWER6_CONTROL_OFF();
	}
}

void TaskForPowerOn(void)
{
	static unsigned char s_Status = 0;
	
	switch(s_Status)
	{
		case 0:
		case 1:
		case 2:
		case 3:
			s_Status++;			//延时20s后开启12V电源	
		break;
		
		case 4:
			s_PowerStatus.PowerStatus1 = 1;
			s_Status++;	
			s_PowerOnTimer = SetTimer(10000, TaskForPowerOn);
//			Task_SetTime(TaskForPowerOn, 50000);
			u1_printf(" Power 1 On\r\n");
		break;
		
		case 5:
			s_PowerStatus.PowerStatus2 = 1;
			s_Status++;	
			u1_printf(" Power 2 On\r\n");
		break;
		
		case 6:
			s_PowerStatus.PowerStatus3 = 1;
			s_Status++;	
			u1_printf(" Power 3 On\r\n");
		break;
		
		case 7:
			s_PowerStatus.PowerStatus4 = 1;
			s_Status++;	
			u1_printf(" Power 4 On\r\n");
		break;
		
		case 8:
			s_PowerStatus.PowerStatus5 = 1;
			s_Status++;	
			u1_printf(" Power 5 On\r\n");
		break;
		
		case 9:
			s_PowerStatus.PowerStatus6 = 1;
			s_Status++;	
			u1_printf(" Power 6 On\r\n");
		break;
		
		case 10:
			u1_printf(" Timer Off\r\n");
			KillTimer(s_PowerOnTimer);
			SetSystemStatus(SYS_NORMAL);
			TX2PowerOn();
		break;
	}
}
void PowerControlPortInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD, ENABLE); //使能端口时钟
	
	GPIO_InitStructure.GPIO_Pin = POWER1_EN_GPIO_PIN ;	         
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;     
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;    
	GPIO_Init(POWER1_EN_GPIO_TYPE, &GPIO_InitStructure);					    
	
	GPIO_InitStructure.GPIO_Pin = POWER2_EN_GPIO_PIN ;	         
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;     
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;    
	GPIO_Init(POWER2_EN_GPIO_TYPE, &GPIO_InitStructure);		
	
	GPIO_InitStructure.GPIO_Pin = POWER3_EN_GPIO_PIN ;	         
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;     
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;    
	GPIO_Init(POWER3_EN_GPIO_TYPE, &GPIO_InitStructure);		
	
	GPIO_InitStructure.GPIO_Pin = POWER4_EN_GPIO_PIN ;	         
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;     
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;    
	GPIO_Init(POWER4_EN_GPIO_TYPE, &GPIO_InitStructure);		
	
	GPIO_InitStructure.GPIO_Pin = POWER5_EN_GPIO_PIN ;	         
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;     
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;    
	GPIO_Init(POWER5_EN_GPIO_TYPE, &GPIO_InitStructure);		
	
	GPIO_InitStructure.GPIO_Pin = POWER6_EN_GPIO_PIN ;	         
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;     
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;    
	GPIO_Init(POWER6_EN_GPIO_TYPE, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = PWRKEY_GPIO_PIN ;	            //端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;      //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //50M
	GPIO_Init(PWRKEY_GPIO_TYPE, &GPIO_InitStructure);					      //根据设定参数初始化GPIO
	
	GPIO_InitStructure.GPIO_Pin = LED_POWER_EN_GPIO_PIN ;	            //端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;      //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //50M
	GPIO_Init(LED_POWER_EN_GPIO_TYPE, &GPIO_InitStructure);					      //根据设定参数初始化GPIO
	
	LED_EN_OUT() = 1;
	
	POWER1_CONTROL_OFF();
	POWER2_CONTROL_OFF();
	POWER3_CONTROL_OFF();
	POWER4_CONTROL_OFF();
	POWER5_CONTROL_OFF();
	POWER6_CONTROL_OFF();
	
	memset(&s_PowerStatus, 0 ,sizeof(s_PowerStatus));
	
	Task_Create(TaskForPowerControl, 10);
	
	s_PowerOnTimer = SetTimer(10000, TaskForPowerOn);
//	Task_Create(TaskForPowerOn, 100000);
}

void CanControlPower(unsigned char Power_Num, unsigned char isOn)
{
	unsigned char u8_Data = 0;
	
	u8_Data = isOn;
	CanProtocolSend(POWER_STATUS_TYPE, Power_Num, DATA_TYPE_U8, (void *)&u8_Data);
}
