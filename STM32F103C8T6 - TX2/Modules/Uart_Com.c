/**********************************
说明:调试使用的串口代码
	  
作者:关宇晟
版本:V2017.4.3
***********************************/
#include "Uart_Com.h"
#include "sys.h"

static unsigned char s_ShowDataFlag = FALSE;

static unsigned char s_ShowDebugBluetoothFlag = FALSE;

unsigned char GetBlutoothDebugFlag(void)
{
	return s_ShowDebugBluetoothFlag;
}


unsigned char GetShowDataFlag(void)
{
	return s_ShowDataFlag;
}

void TaskForTimerTest1(void)
{
	u1_printf("1\r\n");
}
void TaskForTimerTest2(void)
{
	u1_printf("2\r\n");
}
void TaskForTimerTest3(void)
{
	u1_printf("3\r\n");
}
void TaskForTimerTest4(void)
{
	u1_printf("4\r\n");
}
void TaskForTimerTest5(void)
{
	u1_printf("5\r\n");
}
//用于串口交互
void TaskForDebugCOM(void)
{
	unsigned char Num = 0, i, data[100] = {0};

	int ret = 0, i_data = 8877, f_Data = 1081234567;
	float inputdata = 0, outputdata;
	unsigned char LEDColor = 0, LEDStatus = 0, LEDBright = 0;
	unsigned char CanBuf[8] = {0}, CheckSum = 0;
	POWERSTATUS *PowerStatus;
	
	if(g_UartRxFlag == TRUE)
	{
//		u1_printf("Uart1:%s\r\n", g_USART_RX_BUF);
		
		if(strncmp((char *)g_USART_RX_BUF, "Can ", 4) == 0)
		{
			Num = (g_USART_RX_CNT - 3)/3;
			u1_printf("Can Data:");
			for(i=0; i<Num; i++)
			{
				StrToHex(&data[i], &g_USART_RX_BUF[4+i*3], Num);
				u1_printf("%02X ", data[i]);
			}	
			u1_printf("\r\n");
			
			if(Num <= 8)
			{
				ret = Can_Send_Msg(0xA1, data, Num);
				if(ret == 1)
				{
					u1_printf("Can Send fail\r\n");
				}
			}
			else
			{
				u1_printf("Can Num Over\r\n");
			}
			
		}
		else if(strncmp((char *)g_USART_RX_BUF, "can1", 4) == 0)
		{
			CanProtocolSend(RTK_DATA_TYPE, 1, DATA_TYPE_FLOAT, (void *)&f_Data);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "can2", 4) == 0)
		{
			CanProtocolSend(VOL_DATA_TYPE, 0, DATA_TYPE_INT, (void *)&i_data);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "stop", 4) == 0)
		{
			ControlStop();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "mpu", 3) == 0)
		{
			MPU6050_Reset();
		}
//		else if(strncmp((char *)g_USART_RX_BUF, "u3", 2) == 0)
//		{
//			u3_printf("%s", &g_USART_RX_BUF[2]);
//		}
//		else if(strncmp((char *)g_USART_RX_BUF, "u4", 2) == 0)
//		{
//			u4_printf("%s", &g_USART_RX_BUF[2]);
//		}
//		else if(strncmp((char *)g_USART_RX_BUF, "u5", 2) == 0)
//		{
//			printf("%s", &g_USART_RX_BUF[2]);
//		}
		else if(strncmp((char *)g_USART_RX_BUF, "tx2on", g_USART_RX_CNT) == 0)
		{
			u1_printf("TX2 On\r\n");
			TX2PowerOn();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "tx2off", g_USART_RX_CNT) == 0)
		{
			u1_printf("TX2 Power Off\r\n");
			TX2PowerOff();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "poweroff", g_USART_RX_CNT) == 0)
		{
			u1_printf("系统电源关闭\r\n");
			SystemPowerOff();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "12VON", g_USART_RX_CNT) == 0)
		{
			u1_printf("12V Power ON\r\n");
			PowerStatus = GetPowerStatus();
			PowerStatus->PowerStatus1 = 1;
			PowerStatus->PowerStatus2 = 1;
			PowerStatus->PowerStatus3 = 1;
			PowerStatus->PowerStatus4 = 1;
			PowerStatus->PowerStatus5 = 1;
			PowerStatus->PowerStatus6 = 1;
		}
		else if(strncmp((char *)g_USART_RX_BUF, "12VOFF", g_USART_RX_CNT) == 0)
		{
			u1_printf("12V Power Off\r\n");
			PowerStatus = GetPowerStatus();
			PowerStatus->PowerStatus1 = 0;
			PowerStatus->PowerStatus2 = 0;
			PowerStatus->PowerStatus3 = 0;
			PowerStatus->PowerStatus4 = 0;
			PowerStatus->PowerStatus5 = 0;
			PowerStatus->PowerStatus6 = 0;
		}
		else if(strncmp((char *)g_USART_RX_BUF, "showon", g_USART_RX_CNT) == 0)
		{
			u1_printf("Show On\r\n");
			s_ShowDataFlag = TRUE;
		}
		
		else if(strncmp((char *)g_USART_RX_BUF, "showoff", g_USART_RX_CNT) == 0)
		{
			u1_printf("Show Off\r\n");
			s_ShowDataFlag = FALSE;
		}
		else if(strncmp((char *)g_USART_RX_BUF, "timer1", g_USART_RX_CNT) == 0)
		{
			SetTimer(10000, TaskForTimerTest1);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "timer2", g_USART_RX_CNT) == 0)
		{
			SetTimer(10000, TaskForTimerTest2);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "timer3", g_USART_RX_CNT) == 0)
		{
			SetTimer(10000, TaskForTimerTest3);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "timer4", g_USART_RX_CNT) == 0)
		{
			SetTimer(10000, TaskForTimerTest4);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "timer5", g_USART_RX_CNT) == 0)
		{
			SetTimer(10000, TaskForTimerTest5);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "appon", g_USART_RX_CNT) == 0)
		{
			u1_printf("App on\r\n");
			SetAppRunFlag(TRUE);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "appoff", g_USART_RX_CNT) == 0)
		{
			u1_printf("App off\r\n");
			SetAppRunFlag(FALSE);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "pwm", 3) == 0)
		{
			i_data = atoi((char *)&g_USART_RX_BUF[4]);
			u1_printf("Pwm:%d\r\n", i_data);
			TIMER3_CH1_PWM_VALUE = i_data;
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Color ", 6) == 0)
		{
			LEDColor = ArrayToInt32(&g_USART_RX_BUF[6], g_USART_RX_CNT-6);
			u1_printf("Color:%d\r\n", LEDColor);	
			if(LEDColor > 0 && LEDColor < 9)
			{	
				SetLEDColor(LEDColor);
			}
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "Status ", 7) == 0)
		{
			LEDStatus = ArrayToInt32(&g_USART_RX_BUF[7], g_USART_RX_CNT-7);
			u1_printf("LED Status:%d\r\n", LEDStatus);
			if(LEDStatus > 0 && LEDStatus < 8)
			{
				SetLEDStatus(LEDStatus);
			}
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Bright ", 7) == 0)
		{
			LEDBright = ArrayToInt32(&g_USART_RX_BUF[7], g_USART_RX_CNT-7);
			u1_printf("Bright:%d\r\n", LEDBright);	
			if(LEDBright > 0 && LEDBright < 8)
			{	
				SetLEDBright(LEDBright);
			}
		}
		else if(strncmp((char *)g_USART_RX_BUF, "LED ", 4) == 0)
		{
			LEDColor = ArrayToInt32(&g_USART_RX_BUF[4], 1);
			LEDStatus = ArrayToInt32(&g_USART_RX_BUF[6], 1);
			LEDBright = ArrayToInt32(&g_USART_RX_BUF[8], 1);
			u1_printf("LED: Color:%d Status:%d Bright:%d\r\n", LEDColor, LEDStatus, LEDBright);	
			if((LEDColor > 0 && LEDColor < 9) && (LEDStatus > 0 && LEDStatus < 8) && (LEDBright > 0 && LEDBright < 8))
			{	
				u1_printf(" 设置LED状态\r\n");
				SetLEDColor(LEDColor);
				SetLEDStatus(LEDStatus);		
				SetLEDBright(LEDBright);
				memset(CanBuf, 0 ,sizeof(CanBuf));
				CheckSum = 0;
				CanBuf[0] = LED_STATUS_TYPE;
				CanBuf[1] = LEDColor;
				CanBuf[2] = LEDStatus;
				CanBuf[3] = LEDBright;
				for(i=0; i<7; i++)
				{
					CheckSum += CanBuf[i];
				}
				CanBuf[7] = CheckSum;
				Can_Send_Msg(0XA2, CanBuf, 8); 
			}
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Kalman ", 7 ) == 0)
		{
			inputdata = atof((char *)&g_USART_RX_BUF[7]);
			u1_printf("Kalman %.4f\r\n", inputdata);
			outputdata = KalmanFilter(inputdata, 1, 1);
			u1_printf("output:%.4f\r\n", outputdata);
		}
	

		Clear_Uart1Buff();
	}	
}







