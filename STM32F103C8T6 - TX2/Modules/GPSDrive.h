#ifndef		_GPSDRIVE_H_
#define		_GPSDRIVE_H_

#define		GPSPRODUCTINFORMATION		0x01
#define		GPSANTTEST					0x02
#define		GPSCONFING					0x03
#define		GPSGETLOCATION				0x04

typedef __packed struct
{
	int Latitude;
	int Longitude;
	unsigned char RTKStatus;
	unsigned char DataValidity;
}RTKSTRUCT;

RTKSTRUCT* GetRTKData(void);

void SetGPSShowInfo(unsigned char isTrue);

void RTK_Init(void);

#endif



