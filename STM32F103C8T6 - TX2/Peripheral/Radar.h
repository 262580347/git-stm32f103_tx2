#ifndef		_RADAR_H_
#define		_RADAR_H_

typedef __packed struct
{
	int Range1;
	int Range2;
	int Range3;
	int Range4;
}RADARSTRUCT;


void RadarSensorInit(void);

RADARSTRUCT* GetRange(void);


#endif



