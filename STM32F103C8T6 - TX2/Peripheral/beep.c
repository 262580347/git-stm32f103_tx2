#include "sys.h"
#include "beep.h"






void BeepPortInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = BEEP_GPIO_PIN ;	            //端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;      //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //50M
	GPIO_Init(BEEP_GPIO_TYPE, &GPIO_InitStructure);					      //根据设定参数初始化GPIO
}

void BeepRangOnce(void)
{
	BEEP_OUT() = 1;
	
	delay_ms(100);
	
	BEEP_OUT() = 0;
	
	delay_ms(20);
}

void BeepRangTwice(void)
{
	BEEP_OUT() = 1;
	
	delay_ms(50);
	
	BEEP_OUT() = 0;
	
	delay_ms(50);
	
	BEEP_OUT() = 1;
	
	delay_ms(50);
	
	BEEP_OUT() = 0;
	
	delay_ms(20);
}

void BeepRangThreeTimes(void)
{
	BEEP_OUT() = 1;
	
	delay_ms(50);
	
	BEEP_OUT() = 0;
	
	delay_ms(50);

	BEEP_OUT() = 1;
	
	delay_ms(50);
	
	BEEP_OUT() = 0;
	
	delay_ms(50);
	
	BEEP_OUT() = 1;
	
	delay_ms(50);
	
	BEEP_OUT() = 0;
	
	delay_ms(20);
}


