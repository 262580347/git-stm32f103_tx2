#include "TFT_LCD.h"
#include "HY_bmp.h"
#include "sys.h"
#include "oledfont.h"
#include "HY_Logo.h"
#include "WordStock.h"
u32 BACK_COLOR;   //背景色

static unsigned char TFT_Show_Flag = FALSE;

void SetTFT_Show_Flag(unsigned char isTrue)
{
	TFT_Show_Flag = isTrue;
}

/******************************************************************************
      函数说明：LCD串行数据写入函数
      入口数据：dat  要写入的串行数据
      返回值：  无
******************************************************************************/
void LCD_Writ_Bus(u8 dat) 
{	
	u8 i;	
	OLED_CS_Clr();	
	for(i=0;i<8;i++)
	{			  
		OLED_SCLK_Clr();
		if(dat&0x80)
		   OLED_SDIN_Set();
		else 
		   OLED_SDIN_Clr();
		OLED_SCLK_Set();
		dat<<=1;   
	}
	OLED_CS_Set();	
}


/******************************************************************************
      函数说明：LCD写入数据
      入口数据：dat 写入的数据
      返回值：  无
******************************************************************************/
void LCD_WR_DATA8(u8 dat)
{
	OLED_DC_Set();//写数据
	LCD_Writ_Bus(dat);
}


/******************************************************************************
      函数说明：LCD写入数据
      入口数据：dat 写入的数据
      返回值：  无
******************************************************************************/
void LCD_WR_DATA(u32 dat)
{
	OLED_DC_Set();//写数据
	LCD_Writ_Bus(dat>>16);
	LCD_Writ_Bus(dat>>8);
	LCD_Writ_Bus(dat);
}


/******************************************************************************
      函数说明：LCD写入命令
      入口数据：dat 写入的命令
      返回值：  无
******************************************************************************/
void LCD_WR_REG(u8 dat)
{
	OLED_DC_Clr();//写命令
	LCD_Writ_Bus(dat);
}


/******************************************************************************
      函数说明：设置起始和结束地址
      入口数据：x1,x2 设置列的起始和结束地址
                y1,y2 设置行的起始和结束地址
      返回值：  无
******************************************************************************/
void LCD_Address_Set(u32 x1,u32 y1,u32 x2,u32 y2)
{
	LCD_WR_REG(0x2a);//列地址设置
   LCD_WR_DATA8(x1>>8);
   LCD_WR_DATA8(x1);
   LCD_WR_DATA8(x2>>8);
   LCD_WR_DATA8(x2);
   LCD_WR_REG(0x2b);//行地址设置
   LCD_WR_DATA8(y1>>8);
   LCD_WR_DATA8(y1);
   LCD_WR_DATA8(y2>>8);
   LCD_WR_DATA8(y2);
	LCD_WR_REG(0x2c);//储存器写
}


/******************************************************************************
      函数说明：LCD初始化函数
      入口数据：无
      返回值：  无
******************************************************************************/
void Lcd_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	//RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOH, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = POWER_LCD_GPIO_PIN;			//MISO
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;      //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //50M
	GPIO_Init(POWER_LCD_GPIO_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_7;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;      //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //50M
	GPIO_Init(GPIOA, &GPIO_InitStructure);		
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;      //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //50M
	GPIO_Init(GPIOB, &GPIO_InitStructure);					      //根据设定参数初始化GPIO

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;			//MISO
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;      //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //50M
	GPIO_Init(GPIOC, &GPIO_InitStructure);					      //根据设定参数初始化GPIO
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;      //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //50M
	GPIO_Init(GPIOA, &GPIO_InitStructure);					      //根据设定参数初始化GPIO
	
	GPIO_SetBits(POWER_LCD_GPIO_TYPE,POWER_LCD_GPIO_PIN);
	
	delay_ms(200);
	OLED_RST_Clr();
	delay_ms(200);
	OLED_RST_Set();
	delay_ms(20);
	OLED_BLK_Set();
	
//************* Start Initial Sequence **********// 
	LCD_WR_REG(0x11); //Exit Sleep
	delay_ms(60);
	LCD_WR_REG(0XF2);
	LCD_WR_DATA8(0x18);
	LCD_WR_DATA8(0xA3);
	LCD_WR_DATA8(0x12);
	LCD_WR_DATA8(0x02);
	LCD_WR_DATA8(0XB2);
	LCD_WR_DATA8(0x12);
	LCD_WR_DATA8(0xFF);
	LCD_WR_DATA8(0x10);
	LCD_WR_DATA8(0x00);
	LCD_WR_REG(0XF8);
	LCD_WR_DATA8(0x21);
	LCD_WR_DATA8(0x04);
	LCD_WR_REG(0X13);

	LCD_WR_REG(0x36);    // Memory Access Control 
	if(USE_HORIZONTAL==0)LCD_WR_DATA8(0x08);
	else if(USE_HORIZONTAL==1)LCD_WR_DATA8(0xC8);
	else if(USE_HORIZONTAL==2)LCD_WR_DATA8(0x78);
	else LCD_WR_DATA8(0xA8);

	LCD_WR_REG(0xB4);
	LCD_WR_DATA8(0x02);
	LCD_WR_REG(0xB6);
	LCD_WR_DATA8(0x02);
	LCD_WR_DATA8(0x22);
	LCD_WR_REG(0xC1);
	LCD_WR_DATA8(0x41);
	LCD_WR_REG(0xC5);
	LCD_WR_DATA8(0x00);
	LCD_WR_DATA8(0x18);



	LCD_WR_REG(0x3a);
	LCD_WR_DATA8(0x66);
	delay_ms(50);



	LCD_WR_REG(0xE0);
	LCD_WR_DATA8(0x0F);
	LCD_WR_DATA8(0x1F);
	LCD_WR_DATA8(0x1C);
	LCD_WR_DATA8(0x0C);
	LCD_WR_DATA8(0x0F);
	LCD_WR_DATA8(0x08);
	LCD_WR_DATA8(0x48);
	LCD_WR_DATA8(0x98);
	LCD_WR_DATA8(0x37);
	LCD_WR_DATA8(0x0A);
	LCD_WR_DATA8(0x13);
	LCD_WR_DATA8(0x04);
	LCD_WR_DATA8(0x11);
	LCD_WR_DATA8(0x0D);
	LCD_WR_DATA8(0x00);
	LCD_WR_REG(0xE1);
	LCD_WR_DATA8(0x0F);
	LCD_WR_DATA8(0x32);
	LCD_WR_DATA8(0x2E);
	LCD_WR_DATA8(0x0B);
	LCD_WR_DATA8(0x0D);
	LCD_WR_DATA8(0x05);
	LCD_WR_DATA8(0x47);
	LCD_WR_DATA8(0x75);
	LCD_WR_DATA8(0x37);
	LCD_WR_DATA8(0x06);
	LCD_WR_DATA8(0x10);
	LCD_WR_DATA8(0x03);
	LCD_WR_DATA8(0x24);
	LCD_WR_DATA8(0x20);
	LCD_WR_DATA8(0x00);
	LCD_WR_REG(0x11);
	delay_ms(120);
	LCD_WR_REG(0x29);
	LCD_WR_REG(0x2C);
} 


/******************************************************************************
      函数说明：LCD清屏函数
      入口数据：无
      返回值：  无
******************************************************************************/
void LCD_Clear(u32 Color)
{
	u32 i,j;  	
	LCD_Address_Set(0,0,LCD_W-1,LCD_H-1);
    for(i=0;i<LCD_W;i++)
	 {
	  for (j=0;j<LCD_H;j++)
	   	{
        	LCD_WR_DATA(Color);	 			 
	    }
		IWDG_ReloadCounter();

	  }
}

void LCD_Clear_Logg(int Start, int End, u32 Color)
{
	u32 i,j;  	
	LCD_Address_Set(0,Start,LCD_W-1,End);
    for(i=0;i<LCD_W;i++)
	 {
	  for (j=Start;j<End;j++)
	   	{
        	LCD_WR_DATA(Color);	 			 
	    }
		IWDG_ReloadCounter();

	  }
}

/******************************************************************************
      函数说明：LCD显示汉字
      入口数据：x,y   起始坐标
                index 汉字的序号
                size  字号
      返回值：  无
******************************************************************************/
void LCD_ShowChinese32x32(u32 x,u32 y,u8 index,u8 size,u32 color)	
{  
	u8 i,j;
	u8 *temp,size1;
	if(size==16){temp=Hzk16;}//选择字号
	if(size==32){temp=Hzk32;}
  LCD_Address_Set(x,y,x+size-1,y+size-1); //设置一个汉字的区域
  size1=size*size/8;//一个汉字所占的字节
	temp+=index*size1;//写入的起始位置
	for(j=0;j<size1;j++)
	{
		for(i=0;i<8;i++)
		{
		 	if((*temp&(1<<i))!=0)//从数据的低位开始读
			{
				LCD_WR_DATA(color);//点亮
			}
			else
			{
				LCD_WR_DATA(BACK_COLOR);//不点亮
			}
		}
		temp++;
	 }
}


/******************************************************************************
      函数说明：LCD显示汉字
      入口数据：x,y   起始坐标
      返回值：  无
******************************************************************************/
void LCD_DrawPoint(u32 x,u32 y,u32 color)
{
	LCD_Address_Set(x,y,x,y);//设置光标位置 
	LCD_WR_DATA(color);
} 


/******************************************************************************
      函数说明：LCD画一个大的点
      入口数据：x,y   起始坐标
      返回值：  无
******************************************************************************/
void LCD_DrawPoint_big(u32 x,u32 y,u32 color)
{
	LCD_Fill(x-1,y-1,x+1,y+1,color);
} 


/******************************************************************************
      函数说明：在指定区域填充颜色
      入口数据：xsta,ysta   起始坐标
                xend,yend   终止坐标
      返回值：  无
******************************************************************************/
void LCD_Fill(u32 xsta,u32 ysta,u32 xend,u32 yend,u32 color)
{          
	u32 i,j; 
	LCD_Address_Set(xsta,ysta,xend,yend);      //设置光标位置 
	for(i=ysta;i<=yend;i++)
	{													   	 	
		for(j=xsta;j<=xend;j++)LCD_WR_DATA(color);//设置光标位置 	    
	} 					  	    
}


/******************************************************************************
      函数说明：画线
      入口数据：x1,y1   起始坐标
                x2,y2   终止坐标
      返回值：  无
******************************************************************************/
void LCD_DrawLine(u32 x1,u32 y1,u32 x2,u32 y2,u32 color)
{
	u32 t; 
	int xerr=0,yerr=0,delta_x,delta_y,distance;
	int incx,incy,uRow,uCol;
	delta_x=x2-x1; //计算坐标增量 
	delta_y=y2-y1;
	uRow=x1;//画线起点坐标
	uCol=y1;
	if(delta_x>0)incx=1; //设置单步方向 
	else if (delta_x==0)incx=0;//垂直线 
	else {incx=-1;delta_x=-delta_x;}
	if(delta_y>0)incy=1;
	else if (delta_y==0)incy=0;//水平线 
	else {incy=-1;delta_y=-delta_x;}
	if(delta_x>delta_y)distance=delta_x; //选取基本增量坐标轴 
	else distance=delta_y;
	for(t=0;t<distance+1;t++)
	{
		LCD_DrawPoint(uRow,uCol,color);//画点
		xerr+=delta_x;
		yerr+=delta_y;
		if(xerr>distance)
		{
			xerr-=distance;
			uRow+=incx;
		}
		if(yerr>distance)
		{
			yerr-=distance;
			uCol+=incy;
		}
	}
}


/******************************************************************************
      函数说明：画矩形
      入口数据：x1,y1   起始坐标
                x2,y2   终止坐标
      返回值：  无
******************************************************************************/
void LCD_DrawRectangle(u32 x1, u32 y1, u32 x2, u32 y2,u32 color)
{
	LCD_DrawLine(x1,y1,x2,y1,color);
	LCD_DrawLine(x1,y1,x1,y2,color);
	LCD_DrawLine(x1,y2,x2,y2,color);
	LCD_DrawLine(x2,y1,x2,y2,color);
}


/******************************************************************************
      函数说明：画圆
      入口数据：x0,y0   圆心坐标
                r       半径
      返回值：  无
******************************************************************************/
void Draw_Circle(u32 x0,u32 y0,u8 r,u32 color)
{
	int a,b;
	int di;
	a=0;b=r;	  
	while(a<=b)
	{
		LCD_DrawPoint(x0-b,y0-a,color);             //3           
		LCD_DrawPoint(x0+b,y0-a,color);             //0           
		LCD_DrawPoint(x0-a,y0+b,color);             //1                
		LCD_DrawPoint(x0-a,y0-b,color);             //2             
		LCD_DrawPoint(x0+b,y0+a,color);             //4               
		LCD_DrawPoint(x0+a,y0-b,color);             //5
		LCD_DrawPoint(x0+a,y0+b,color);             //6 
		LCD_DrawPoint(x0-b,y0+a,color);             //7
		a++;
		if((a*a+b*b)>(r*r))//判断要画的点是否过远
		{
			b--;
		}
	}
}


/******************************************************************************
      函数说明：显示字符
      入口数据：x,y    起点坐标
                num    要显示的字符
                mode   1叠加方式  0非叠加方式
      返回值：  无
******************************************************************************/
void LCD_ShowChar(u32 x,u32 y,u8 num,u8 mode,u32 color)
{
    u8 temp;
    u8 pos,t;
	  u32 x0=x;     
    if(x>LCD_W-16||y>LCD_H-16)return;	    //设置窗口		   
	num=num-' ';//得到偏移后的值
	LCD_Address_Set(x,y,x+8-1,y+16-1);      //设置光标位置 
	if(!mode) //非叠加方式
	{
		for(pos=0;pos<16;pos++)
		{ 
			temp=asc2_1608[(u32)num*16+pos];		 //调用1608字体
			for(t=0;t<8;t++)
		    {                 
		        if(temp&0x01)LCD_WR_DATA(color);
				    else LCD_WR_DATA(BACK_COLOR);
				    temp>>=1; 
				    x++;
		    }
			x=x0;
			y++;
		}	
	}else//叠加方式
	{
		for(pos=0;pos<16;pos++)
		{
		    temp=asc2_1608[(u32)num*16+pos];		 //调用1608字体
			for(t=0;t<8;t++)
		    {                 
		        if(temp&0x01)LCD_DrawPoint(x+t,y+pos,color);//画一个点     
		        temp>>=1; 
		    }
		}
	}   	   	 	  
}


/******************************************************************************
      函数说明：显示字符串
      入口数据：x,y    起点坐标
                *p     字符串起始地址
      返回值：  无
******************************************************************************/
void LCD_ShowString(u32 x,u32 y,const u8 *p,u32 color)
{         
    while(*p!='\0')
    {       
        if(x>LCD_W-16){x=0;y+=16;}
        if(y>LCD_H-16){y=x=0;LCD_Clear(RED);}
        LCD_ShowChar(x,y,*p,0,color);
        x+=8;
        p++;
    }  
}


/******************************************************************************
      函数说明：显示数字
      入口数据：m底数，n指数
      返回值：  无
******************************************************************************/
u32 mypow(u8 m,u8 n)
{
	u32 result=1;	 
	while(n--)result*=m;    
	return result;
}


/******************************************************************************
      函数说明：显示数字
      入口数据：x,y    起点坐标
                num    要显示的数字
                len    要显示的数字个数
      返回值：  无
******************************************************************************/
void LCD_ShowNum(u32 x,u32 y,u32 num,u8 len,u32 color)
{         	
	u8 t,temp;
	u8 enshow=0;
	for(t=0;t<len;t++)
	{
		temp=(num/mypow(10,len-t-1))%10;
		if(enshow==0&&t<(len-1))
		{
			if(temp==0)
			{
				LCD_ShowChar(x+8*t,y,' ',0,color);
				continue;
			}else enshow=1; 
		 	 
		}
	 	LCD_ShowChar(x+8*t,y,temp+48,0,color); 
	}
} 

void LCD_ShowNum0(u32 x,u32 y,u32 num,u8 len,u32 color)
{         	
	u8 t,temp;
	u8 enshow=0;
	for(t=0;t<len;t++)
	{
		temp=(num/mypow(10,len-t-1))%10;
		if(enshow==0&&t<(len-1))
		{
			if(temp==0)
			{
				LCD_ShowChar(x+8*t,y,'0',0,color);
				continue;
			}else enshow=1; 
		 	 
		}
	 	LCD_ShowChar(x+8*t,y,temp+48,0,color); 
	}
} 
/******************************************************************************
      函数说明：显示小数
      入口数据：x,y    起点坐标
                num    要显示的小数
                len    要显示的数字个数
      返回值：  无
******************************************************************************/
void LCD_ShowNum1(u32 x,u32 y,float num,u8 len,u32 color)
{         	
	u8 t,temp;
	u8 enshow=0;
	u32 num1;
	num1=num*100;
	for(t=0;t<len;t++)
	{
		temp=(num1/mypow(10,len-t-1))%10;
		if(t==(len-2))
		{
			LCD_ShowChar(x+8*(len-2),y,'.',0,color);
			t++;
			len+=1;
		}
	 	LCD_ShowChar(x+8*t,y,temp+48,0,color);
	}
}

void Lcd_printf(u32 x,u32 y, char* fmt,...)  
{  
	u16 Len;
	char LcdBuf[256];
	va_list ap;
	   
	memset(LcdBuf, 0, sizeof(LcdBuf));
	va_start(ap,fmt);
	vsprintf((char*)LcdBuf,fmt,ap);
	va_end(ap);
	
	Len = strlen((const char*)LcdBuf);//此次发送数据的长度	
	
	LcdBuf[Len] = '\0';
	
	LCD_ShowString(x,y,(const u8*)LcdBuf,RED);
}


/******************************************************************************
      函数说明：显示40x40图片,i的值为X*Y的像素值
      入口数据：x,y    起点坐标
      返回值：  无
******************************************************************************/
void LCD_ShowPicture(u32 x1,u32 y1,u32 x2,u32 y2)
{
	int i;
//	  LCD_Address_Set(x1,y1,x2,y2);
//	for(i=0;i<10000;i++)
//	  { 	
//			LCD_WR_DATA8(gImage_HY_Logo[i*3+2]);
//			LCD_WR_DATA8(gImage_HY_Logo[i*3+1]);
//			LCD_WR_DATA8(gImage_HY_Logo[i*3]);		
//	  }	

	  LCD_Address_Set(x1,y1,x2,y2);
		for(i=0;i<20800;i++)
	  { 	
			LCD_WR_DATA8(gImage_HY_bmp[i*3+2]);
			LCD_WR_DATA8(gImage_HY_bmp[i*3+1]);
			LCD_WR_DATA8(gImage_HY_bmp[i*3]);		
	  }				
}


void LCD_ShowHYlogo(void)
{
	int i;
	LCD_Address_Set(380,0,479,99);
	for(i=0;i<10000;i++)
	{ 	
		LCD_WR_DATA8(gImage_HY_Logo[i*3+2]);
		LCD_WR_DATA8(gImage_HY_Logo[i*3+1]);
		LCD_WR_DATA8(gImage_HY_Logo[i*3]);		
	}	

			
}

static void TaskForLCDControl(void)
{
	static unsigned char s_First = FALSE;
	char str[100];
	RTKSTRUCT *p_RTKData;
	RADARSTRUCT	*p_RadarRange;
	ImuType* p_ImuDataOut;
	int DCPowerVol = 0;
	MasterDataType* p_MasterData;
	

	if(TFT_Show_Flag  == FALSE)
	{
		if(s_First == TRUE)
		{
			s_First = FALSE;
			LCD_Clear(BLUE1);
			LCD_ShowPicture(110,120,369,199);
		}
		return;
	}
	
	if(s_First == FALSE)
	{
		s_First = TRUE;
		LCD_Clear(WHITE);
		Display_GB2312_String(40,16,2, "慧云信息智能巡园车",RED); //24x24汉字
		Display_GB2312_String(40,16,2, "慧云信息智能巡园车",RED); //24x24汉字
		LCD_ShowString(200,16,"LAN IP:",RED);
		
		Display_GB2312_String(40,40,2, "电压：",RED); //24x24汉字		
		LCD_ShowString(200,40,"WLAN IP:",RED);
		LCD_ShowString(120,40,"mV",RED);
		
		Display_GB2312_String(40,64,2, "障碍物距离：",RED); //24x24汉字
		
		LCD_ShowString(40,112,"Imu:",RED);
		
		LCD_ShowString(40,160,"RTK:",RED);
		
		Display_GB2312_String(40,208,2, "机械避障传感器：",RED); //24x24汉字
		
		LCD_ShowHYlogo();
	}
	p_RadarRange = GetRange();
	p_RTKData = GetRTKData();
	p_ImuDataOut = GetImuDataOut();
	DCPowerVol = GetDCPowerVol();
	p_MasterData = GetMasterData();

	LCD_ShowString(256,16,p_MasterData->LAN_IP,BLUE);
	LCD_ShowString(264,40,p_MasterData->WLAN_IP,BLUE);
	
	LCD_ShowNum(80, 40, DCPowerVol, 5, BLUE);
	sprintf(str, "1:%d  2:%d  3:%d  4:%d    ", p_RadarRange->Range1, p_RadarRange->Range2, p_RadarRange->Range3, p_RadarRange->Range4 );
	LCD_ShowString(40,88,str,BLUE);
	
	sprintf(str, "X:%.1f Y:%.1f Z:%.1f %.1f%%    ", p_ImuDataOut->angular_x, p_ImuDataOut->angular_y, p_ImuDataOut->angular_z, p_ImuDataOut->MageneticRatio);
	LCD_ShowString(40,136,str,BLUE);

	sprintf(str, "%d, %d   %d", p_RTKData->Latitude, p_RTKData->Longitude, p_RTKData->RTKStatus);
	LCD_ShowString(40,184,str,BLUE);
	
	if((GetKeyStatus() & 0x04))
	{
		LCD_ShowString(40,232,"N",BLUE);
	}
	else
	{
		LCD_ShowString(40,232,"Y",BLUE);
	}
	
	if((GetKeyStatus() & 0x02))
	{
		LCD_ShowString(56,232,"N",BLUE);
	}
	else
	{
		LCD_ShowString(56,232,"Y",BLUE);
	}
	
	if((GetKeyStatus() & 0x01))
	{
		LCD_ShowString(72,232,"N",BLUE);
	}
	else
	{
		LCD_ShowString(72,232,"Y",BLUE);
	}
}

void LCD_TFT_Init(void)
{
	u8 i, m;
	
	Lcd_Init();
	
	u1_printf("\r\n [LCD] Init Finish\r\n");		//初始化OLED  
	LCD_Clear(BLUE1);
	BACK_COLOR=WHITE;

//		LCD_ShowChinese32x32(10,0,0,32,RED);   //中
//		LCD_ShowChinese32x32(45,0,1,32,RED);   //景
//		LCD_ShowChinese32x32(80,0,2,32,RED);   //园
//		LCD_ShowChinese32x32(115,0,3,32,RED);  //电
//		LCD_ShowChinese32x32(150,0,4,32,RED);  //子
//	 
//		LCD_ShowChinese32x32(10,75,0,16,RED);   //中
//		LCD_ShowChinese32x32(45,75,1,16,RED);   //景
//		LCD_ShowChinese32x32(80,75,2,16,RED);   //园
//		LCD_ShowChinese32x32(115,75,3,16,RED);  //电
//		LCD_ShowChinese32x32(150,75,4,16,RED);  //子
//		LCD_ShowString(10,35,"2.4 TFT SPI 240*320",RED);
//		LCD_ShowString(10,55,"LCD_W:",RED);	LCD_ShowNum(70,55,LCD_W,3,RED);
//		LCD_ShowString(110,55,"LCD_H:",RED);LCD_ShowNum(160,55,LCD_H,3,RED);
//		LCD_ShowNum1(80,95,12.342,5,RED);
//	  for(i=0;i<5;i++)
//	  {
//			for(m=0;m<6;m++)
//			{
//			  LCD_ShowPicture(0+m*40,120+i*40,39+m*40,159+i*40);
//			}
//	  }
//		Display_Asc_String(0,55,1, "ASCII_5x7",RED);//ASC 5X7点阵
//		Display_Asc_String(0,65,2, "ASCII_7x8",RED);  //ASC 7X8点阵
//		Display_Asc_String(0,80,3, "ASCII_6x12",RED);	//ASC 6X12点阵
//		Display_Asc_String(0,100,4, "ASCII_8x16",RED);	//ASC 8X16点阵
//		Display_Asc_String(0,120,5, "ASCII_12x24",RED);//ASC 12X24点阵
//		Display_Asc_String(0,160,6, "ASCII_16x32",RED);	 //ASC 16X32点阵  
//		Display_GB2312_String(0,200,1, "中景园１２ｘ１２",RED);//12x12汉字
//		Display_GB2312_String(0,220,2, "中景园１６ｘ１６",RED);//15x16汉字
//		Display_GB2312_String(0,240,3, "中景园２４ｘ２４",RED); //24x24汉字
//		Display_GB2312_String(0,270,4, "中景园３２３２",RED); //32x32汉字

//	Display_GB2312_String(48,16,2, "慧云信息智能巡园车",RED); //24x24汉字
//	Display_GB2312_String(48,16,2, "慧云信息智能巡园车",RED); //24x24汉字
//	LCD_ShowString(240,16,"GYS Test Device",RED);

//	Display_GB2312_String(40,40,2, "电压：",RED); //24x24汉字
//		
//	LCD_ShowString(120,40,"mV",RED);
	
//	LCD_ShowPicture(380,0,479,99);
//	LCD_ShowPicture(190,110,289,209);
	LCD_ShowPicture(110,120,369,199);
	
	Task_Create(TaskForLCDControl, 50000);
}



















