#ifndef _WS2812_SPI_H_
#define _WS2812_SPI_H_

//#include "stm32f10x.h"
#include "sys.h"


#define SPI1_BYTE_LEN_PER_PIXEL			3
#define SPI2_BYTE_LEN_PER_PIXEL			3

#define 	PIXEL_NUM 		8		//有多少LED需要控制

#define		RGBMAX		20			//列数

#define WS2812_IN_PIN	PBout(15)



typedef struct{
	uint8_t R_Val;
	uint8_t G_Val;
	uint8_t B_Val;
}T_RGB_CTRL;


int8_t WS2812_Send24Bits(T_RGB_CTRL *sRGBVal);
uint32_t WS2812_Pixels_Packet(uint32_t pixel_num,uint32_t *rgb_buf,uint8_t* spi_packet_buf);

#endif

