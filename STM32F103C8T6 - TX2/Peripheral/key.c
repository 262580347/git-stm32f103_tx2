#include "key.h"

#define		MCU_KEY_PIN				GPIO_Pin_8
#define		MCU_KEY_TYPE			GPIOA

#define		MCU_DET_PIN				GPIO_Pin_13
#define		MCU_DET_TYPE			GPIOB

#define		TX2_CTR_PIN				GPIO_Pin_14
#define		TX2_CTR_TYPE			GPIOB

#define		MCUKEY_VALUE				1
#define		MCUKEY_CONTINUE_VALUE		2

#define		READ_MCUKEY_UP()			GPIO_ReadInputDataBit(MCU_KEY_TYPE,MCU_KEY_PIN)
#define		READ_MCUKEY_DOWN()			GPIO_ReadInputDataBit(KEY_DOWN_TYPE,KEY_DOWN_PIN)

#define		READ_MCUDET_UP()			GPIO_ReadInputDataBit(MCU_DET_TYPE,MCU_DET_PIN)
#define		READ_MCUDET_DOWN()			GPIO_ReadInputDataBit(MCU_DET_TYPE,MCU_DET_PIN)

#define		LEVEL_LOW			Bit_RESET
#define		LEVEL_HIGH			Bit_SET


unsigned char KeyBoardScan(unsigned char ContinueFlag)
{
	static u8 KeyPressStatus = TRUE;//按键按松开标志
	u32 TimeCount = 0;
	
	if(ContinueFlag)
	{
		KeyPressStatus = TRUE;  //支持连按		  
	}
	
	if(KeyPressStatus && (READ_MCUDET_UP() == LEVEL_LOW))
	{
		delay_ms(10);//去抖动 
		
		KeyPressStatus = FALSE;
		
//		if(READ_MCUDET_UP() == LEVEL_LOW)
//		{
//			return MCUKEY_VALUE;
//		}
		
		if(READ_MCUDET_UP() == LEVEL_LOW)
		{
			TimeCount = GetSystem10msCount();
			
			while(READ_MCUDET_UP() == LEVEL_LOW)
			{
				if(CalculateTime(GetSystem10msCount(), TimeCount) >= 150)
				{
					return MCUKEY_CONTINUE_VALUE;
				}
			}
			return MCUKEY_VALUE;
		}
	}
	else if(READ_MCUDET_UP() == LEVEL_HIGH)
	{
		KeyPressStatus = TRUE; 	    
	}
 	return 0;// 无按键按下
}

void TaskForKeyDetect(void)
{
	u8 KeyValue = 0;
	
	KeyValue = KeyBoardScan(FALSE);
	
	switch(KeyValue)
	{
		case MCUKEY_VALUE:
			u1_printf(" MCU Key 被按下\r\n");		
		break;
		
		case MCUKEY_CONTINUE_VALUE:
			if(GetSystemStatus() == SYS_NORMAL)
			{
				u1_printf("开始关闭TX2\r\n");
				TX2PowerOff();
				SetSystemStatus(SYS_SHUTINGDOWN);
			}
			else
			{
				u1_printf("正在开机或者正在关机，不重复执行\r\n");
			}
		break;
		
		default:
			
		break;
	}
}
/**************************************************************************
函数功能：按键初始化
入口参数：无
返回  值：无 
**************************************************************************/
void KEY_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = MCU_KEY_PIN;	            //端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;      //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //50M
	GPIO_Init(MCU_KEY_TYPE, &GPIO_InitStructure);					      //根据设定参数初始化GPIO
	
	MCU_KEY_OUT() = 1;
	
	GPIO_InitStructure.GPIO_Pin = MCU_DET_PIN;	            //端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;      //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //50M
	GPIO_Init(MCU_DET_TYPE, &GPIO_InitStructure);					      //根据设定参数初始化GPIO
	
	GPIO_InitStructure.GPIO_Pin = TX2_CTR_PIN;	            //端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;      //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //50M
	GPIO_Init(TX2_CTR_TYPE, &GPIO_InitStructure);					      //根据设定参数初始化GPIO
	
	TX2_CTR_OUT() = 0;
	
	Task_Create(TaskForKeyDetect, 10);
	
} 


