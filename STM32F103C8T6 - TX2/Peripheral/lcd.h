#ifndef __LCD_H
#define __LCD_H			  	 
#include "sys.h"
#include "stdlib.h"	   
#define USE_HORIZONTAL 0  //���ú�������������ʾ 0��1Ϊ���� 2��3Ϊ����


#if USE_HORIZONTAL==0||USE_HORIZONTAL==1
#define LCD_W 320
#define LCD_H 480

#else
#define LCD_W 480
#define LCD_H 320
#endif
#define	u8 unsigned char
#define	u16 unsigned int
#define	u32 unsigned long
   						  
//-----------------����LED�˿ڶ���---------------- 

#define LED_ON GPIO_ResetBits(GPIOA,GPIO_Pin_15)
#define LED_OFF GPIO_SetBits(GPIOA,GPIO_Pin_15)

//-----------------OLED�˿ڶ���---------------- 

#define OLED_SCLK_Clr() GPIO_ResetBits(GPIOA,GPIO_Pin_0)
#define OLED_SCLK_Set() GPIO_SetBits(GPIOA,GPIO_Pin_0)

#define OLED_SDIN_Clr() GPIO_ResetBits(GPIOA,GPIO_Pin_1)//DIN
#define OLED_SDIN_Set() GPIO_SetBits(GPIOA,GPIO_Pin_1)

#define OLED_RST_Clr() GPIO_ResetBits(GPIOA,GPIO_Pin_2)//RES
#define OLED_RST_Set() GPIO_SetBits(GPIOA,GPIO_Pin_2)

#define OLED_DC_Clr() GPIO_ResetBits(GPIOA,GPIO_Pin_3)//DC
#define OLED_DC_Set() GPIO_SetBits(GPIOA,GPIO_Pin_3)

#define OLED_BLK_Clr()  GPIO_ResetBits(GPIOA,GPIO_Pin_4)//BLK
#define OLED_BLK_Set()  GPIO_SetBits(GPIOA,GPIO_Pin_4)

#define OLED_CS_Clr()  GPIO_ResetBits(GPIOA,GPIO_Pin_6)//CS1 SPIƬѡ
#define OLED_CS_Set()  GPIO_SetBits(GPIOA,GPIO_Pin_6)

//----------------------------------------------------------------------------
#define ZK_CS_Clr()  GPIO_ResetBits(GPIOA,GPIO_Pin_7)//CS2 �ֿ�Ƭѡ
#define ZK_CS_Set()  GPIO_SetBits(GPIOA,GPIO_Pin_7)					
	
#define ZK_OUT()    GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_5)//MISO  ��ȡ�ֿ���������
//--------------------------------------------------------------------------------- 

#define OLED_CMD  0	//д����
#define OLED_DATA 1	//д����

extern  u16 BACK_COLOR;   //����ɫ

void LCD_Writ_Bus(u8 dat);
void LCD_WR_DATA8(u8 dat);
void LCD_WR_DATA(u16 dat);
void LCD_WR_REG(u8 dat);
void LCD_Address_Set(u16 x1,u16 y1,u16 x2,u16 y2);
void Lcd_Init(void); 
void LCD_Clear(u16 Color);
void LCD_ShowChinese32x32(u16 x,u16 y,u8 index,u8 size,u16 color);
void LCD_DrawPoint(u16 x,u16 y,u16 color);
void LCD_DrawPoint_big(u16 x,u16 y,u16 colory);
void LCD_Fill(u16 xsta,u16 ysta,u16 xend,u16 yend,u16 color);
void LCD_DrawLine(u16 x1,u16 y1,u16 x2,u16 y2,u16 color);
void LCD_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2,u16 color);
void Draw_Circle(u16 x0,u16 y0,u8 r,u16 color);
void LCD_ShowChar(u16 x,u16 y,u8 num,u8 mode,u16 color);
void LCD_ShowString(u16 x,u16 y,const u8 *p,u16 color);
u32 mypow(u8 m,u8 n);
void LCD_ShowNum(u16 x,u16 y,u16 num,u8 len,u16 color);
void LCD_ShowNum1(u16 x,u16 y,float num,u8 len,u16 color);
void LCD_ShowPicture(u16 x1,u16 y1,u16 x2,u16 y2);

void CL_Mem(void);
void ZK_command(u8 dat);
u8  get_data_from_ROM(void);
void get_n_bytes_data_from_ROM(u8 AddrHigh,u8 AddrMid,u8 AddrLow,u8 *pBuff,u8 DataLen);
void Display_GB2312(u16 x,u16 y,u8 zk_num,u16 color);
void Display_GB2312_String(u16 x,u16 y,u8 zk_num,u8 text[],u16 color);
void Display_Asc(u16 x,u16 y,u8 zk_num,u16 color);
void Display_Asc_String(u16 x,u8 y,u16 zk_num,u8 text[],u16 color);


//������ɫ
#define WHITE         	 0xFCFCFC
#define BLACK            0X000000
#define RED           	 0xFC0000
#define GREEN            0x00FC00
#define BLUE             0x0000FC
					  		 
#endif  
	 
	 



