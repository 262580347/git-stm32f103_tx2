#ifndef _BEEP_H_
#define	_BEEP_H_


void BeepPortInit(void);

void BeepRangOnce(void);

void BeepRangTwice(void);

void BeepRangThreeTimes(void);


#endif


