#include "sys.h"
#include "Radar.h"

#define		BUF_SIZE		5
#define		MAX_SPEED_VALUE		1500
#define		DATA_HEAD		0X55
RADARSTRUCT s_RadarRange;

RADARSTRUCT* GetRange(void)
{
	return &s_RadarRange;
}

static void RadarDataComunication(void)
{
	int Range1, Range2, Range3, Range4 = 0;
	static int RangeBuf1[BUF_SIZE] = {0}, RangeBuf2[BUF_SIZE] = {0}, RangeBuf3[BUF_SIZE] = {0}, RangeBuf4[BUF_SIZE] = {0};
	static int DetectCount = 0;
	static unsigned char s_StartCalFlag = FALSE;
	static unsigned char WarningCount = 0;
	
	Range1 = s_RadarRange.Range1;
	Range2 = s_RadarRange.Range2;
	Range3 = s_RadarRange.Range3;
	Range4 = s_RadarRange.Range4;
	
	RangeBuf1[DetectCount] = Range1;
	RangeBuf2[DetectCount] = Range2;
	RangeBuf3[DetectCount] = Range3;
	RangeBuf4[DetectCount] = Range4;
	DetectCount++;
	if(DetectCount >= BUF_SIZE)
	{
		DetectCount = 0;
		s_StartCalFlag = TRUE;
	}
	
	if(s_StartCalFlag)
	{
		Range1 = Mid_Filter_int(RangeBuf1, BUF_SIZE);
		Range2 = Mid_Filter_int(RangeBuf2, BUF_SIZE);
		Range3 = Mid_Filter_int(RangeBuf3, BUF_SIZE);
		Range4 = Mid_Filter_int(RangeBuf4, BUF_SIZE);
		Range1 = KalmanFilter(Range1, 5, 10);
		Range2 = KalmanFilter(Range2, 5, 10);
		Range3 = KalmanFilter(Range3, 5, 10);
		Range4 = KalmanFilter(Range4, 5, 10);

	}
	
	if((Range1 > 30 && Range1 < 500) || (Range2 > 30 && Range2 < 500) || (Range3 > 30 && Range3 < 500) || (Range4 > 30 && Range4 < 500))
	{
		u1_printf("%d %d %d %d!!!!!!!!!\r\n", Range1, Range2, Range3, Range4);
		POWER1_CONTROL_ON();

	}
	else
	{
		u1_printf("%d %d %d %d\r\n", Range1, Range2, Range3, Range4);
		WarningCount = 0;
		POWER1_CONTROL_OFF();
	}
}

static void TaskForRadar(void)
{
	unsigned char CheckSum = 0;
	unsigned char DataHead = 0, i;
	unsigned char DataBuf[10] = {0};
	
	if(g_Uart5RxFlag == TRUE)
	{
		DataHead = g_USART5_RX_BUF[0];
		if(DataHead == DATA_HEAD && g_USART5_RX_CNT >= 10)
		{
			memcpy(DataBuf, g_USART5_RX_BUF, 10);
			for(i=0; i<9; i++)
			{
				CheckSum += DataBuf[i];
			}
			
			if(CheckSum == DataBuf[9])
			{
				s_RadarRange.Range1 = (DataBuf[1] << 8) + DataBuf[2];
				s_RadarRange.Range2 = (DataBuf[3] << 8) + DataBuf[4];
				s_RadarRange.Range3 = (DataBuf[5] << 8) + DataBuf[6];
				s_RadarRange.Range4 = (DataBuf[7] << 8) + DataBuf[8];
				
				RadarDataComunication();
			}
			else
			{
				u1_printf("CheckSum Err\r\n");
			}
			
		}
		else
		{
			u1_printf("Head Err\r\n");
			
		}
		Clear_Uart5Buff();
	}
}

void RadarSensorInit(void)
{
	u1_printf("Radar Sensor Init...\r\n");
	Task_Create(TaskForRadar, 100);
}
