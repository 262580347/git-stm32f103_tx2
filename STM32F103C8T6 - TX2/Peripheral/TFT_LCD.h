#ifndef		_TFT_LCD_H_
#define		_TFT_LCD_H_

//#define	unsigned char unsigned char
//#define	unsigned int unsigned int
//#define	unsigned int unsigned long
	
//#define		LCD_CLK_PIN		GPIO_Pin_10
//#define		LCD_CLK_TYPE	GPIOB

//#define		LCD_MOSI_PIN	GPIO_Pin_10
//#define		LCD_MOSI_TYPE	GPIOB

//#define		LCD_RES_PIN		GPIO_Pin_10
//#define		LCD_RES_TYPE	GPIOB

//#define		LCD_DC_PIN		GPIO_Pin_10
//#define		LCD_DC_TYPE		GPIOA

//#define		LCD_BLK_PIN		GPIO_Pin_10
//#define		LCD_BLK_TYPE	GPIOA

//#define		LCD_MISO_PIN	GPIO_Pin_10
//#define		LCD_MISO_TYPE	GPIOA

//#define		LCD_CS1_PIN		GPIO_Pin_10
//#define		LCD_CS1_TYPE	GPIOA

//#define		LCD_CS2_PIN		GPIO_Pin_10
//#define		LCD_CS2_TYPE	GPIOA

#define USE_HORIZONTAL 2  //���ú�������������ʾ 0��1Ϊ���� 2��3Ϊ����


#if USE_HORIZONTAL==0 || USE_HORIZONTAL==1
#define LCD_W 320
#define LCD_H 480

#else
#define LCD_W 480
#define LCD_H 320
#endif

   						  
//-----------------����LED�˿ڶ���---------------- 

//#define LED_ON GPIO_ResetBits(GPIOB,GPIO_Pin_6)
//#define LED_OFF GPIO_SetBits(GPIOB,GPIO_Pin_6)

//-----------------OLED�˿ڶ���---------------- 

#define OLED_SCLK_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_1)	//SCLK
#define OLED_SCLK_Set() GPIO_SetBits(GPIOB,GPIO_Pin_1)

#define OLED_SDIN_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_0)//DIN	MISO
#define OLED_SDIN_Set() GPIO_SetBits(GPIOB,GPIO_Pin_0)

#define OLED_RST_Clr() 	GPIO_ResetBits(GPIOC,GPIO_Pin_5)//RES
#define OLED_RST_Set() 	GPIO_SetBits(GPIOC,GPIO_Pin_5)

#define OLED_DC_Clr() 	GPIO_ResetBits(GPIOC,GPIO_Pin_4)//DC
#define OLED_DC_Set() 	GPIO_SetBits(GPIOC,GPIO_Pin_4)

#define OLED_BLK_Clr()  GPIO_ResetBits(GPIOA,GPIO_Pin_7)//BLK
#define OLED_BLK_Set()  GPIO_SetBits(GPIOA,GPIO_Pin_7)

#define ZK_OUT()    	GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_6)//MISO  ��ȡ�ֿ���������

#define OLED_CS_Clr()  	GPIO_ResetBits(GPIOA,GPIO_Pin_5)//CS1 SPIƬѡ
#define OLED_CS_Set()  	GPIO_SetBits(GPIOA,GPIO_Pin_5)

//----------------------------------------------------------------------------
#define ZK_CS_Clr()  	GPIO_ResetBits(GPIOA,GPIO_Pin_4)//CS2 �ֿ�Ƭѡ
#define ZK_CS_Set()  	GPIO_SetBits(GPIOA,GPIO_Pin_4)					
	
#define		POWER_LCD_GPIO_TYPE		GPIOB
#define		POWER_LCD_GPIO_PIN		GPIO_Pin_12
//--------------------------------------------------------------------------------- 

#define OLED_CMD  0	//д����
#define OLED_DATA 1	//д����

void LCD_TFT_Init(void);

extern  unsigned int BACK_COLOR;   //����ɫ

void LCD_Writ_Bus(unsigned char dat);
void LCD_WR_DATA8(unsigned char dat);
void LCD_WR_DATA(unsigned int dat);
void LCD_WR_REG(unsigned char dat);
void LCD_Address_Set(unsigned int x1,unsigned int y1,unsigned int x2,unsigned int y2);
void Lcd_Init(void); 
void LCD_Clear(unsigned int Color);
void LCD_ShowChinese32x32(unsigned int x,unsigned int y,unsigned char index,unsigned char size,unsigned int color);
void LCD_DrawPoint(unsigned int x,unsigned int y,unsigned int color);
void LCD_DrawPoint_big(unsigned int x,unsigned int y,unsigned int colory);
void LCD_Fill(unsigned int xsta,unsigned int ysta,unsigned int xend,unsigned int yend,unsigned int color);
void LCD_DrawLine(unsigned int x1,unsigned int y1,unsigned int x2,unsigned int y2,unsigned int color);
void LCD_DrawRectangle(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2,unsigned int color);
void Draw_Circle(unsigned int x0,unsigned int y0,unsigned char r,unsigned int color);
void LCD_ShowChar(unsigned int x,unsigned int y,unsigned char num,unsigned char mode,unsigned int color);
void LCD_ShowString(unsigned int x,unsigned int y,const unsigned char *p,unsigned int color);
unsigned int mypow(unsigned char m,unsigned char n);
void LCD_ShowNum(unsigned int x,unsigned int y,unsigned int num,unsigned char len,unsigned int color);
void LCD_ShowNum1(unsigned int x,unsigned int y,float num,unsigned char len,unsigned int color);
void LCD_ShowPicture(unsigned int x1,unsigned int y1,unsigned int x2,unsigned int y2);

void CL_Mem(void);
void ZK_command(unsigned char dat);
unsigned char  get_data_from_ROM(void);
void get_n_bytes_data_from_ROM(unsigned char AddrHigh,unsigned char AddrMid,unsigned char AddrLow,unsigned char *pBuff,unsigned char DataLen);
void Display_GB2312(unsigned int x,unsigned int y,unsigned char zk_num,unsigned int color);
void Display_GB2312_String(unsigned int x,unsigned int y,unsigned char zk_num,unsigned char text[],unsigned int color);
void Display_Asc(unsigned int x,unsigned int y,unsigned char zk_num,unsigned int color);
void Display_Asc_String(unsigned int x,unsigned char y,unsigned int zk_num,unsigned char text[],unsigned int color);

void Lcd_printf(unsigned int x,unsigned int y, char* fmt,...) ;

void SetTFT_Show_Flag(unsigned char isTrue);
	
//������ɫ
#define WHITE         	 0xFCFCFC
#define BLACK            0X000000
#define RED           	 0xFC0000
#define GREEN            0x00FC00
#define BLUE             0x0000FC

#define BLUE1  			 0X3F48CC
#endif


