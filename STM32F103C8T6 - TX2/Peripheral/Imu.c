#include "Imu.h"
#include "sys.h"

#define		MAX_ANGULAR_OFFSET		10	

static float Imu1_MageneticRatio = 0, Imu2_MageneticRatio = 0, Imu3_MageneticRatio = 0;
static unsigned char Imu1TimeOutFlag = FALSE, Imu2TimeOutFlag = FALSE, Imu3TimeOutFlag = FALSE;

static ImuType ImuData1;
static ImuType ImuData2;
static ImuType ImuData3;
static ImuType ImuDataOut, ImuDataSave;

static unsigned char Imu1StatusFlag = FALSE, Imu2StatusFlag = FALSE, Imu3StatusFlag = FALSE;

#define		MEGNETIC_MAX		37.5

ImuType* GetImuDataOut(void)
{
	return &ImuDataSave;
}

void TaskForImu1(void)
{
	int i;
	unsigned char Rec_Buf[100] = {0};

	static unsigned char Count = 0;
	unsigned char CheckSumAcceleration = 0, CheckSumAngularVelocity = 0, CheckSumAngular = 0, CheckSumMagneticField = 0, CheckSumOrientation = 0;   //计算出的校验和
	unsigned char RecCheckSumAcceleration = 0, RecCheckSumAngularVelocity = 0, RecCheckSumAngular = 0, RecCheckSumAngularMagneticField = 0, RecCheckSumOrientation = 0;  //接收到的校验和
	short linear_acceleration_x, linear_acceleration_y, linear_acceleration_z; //加速度
	short angular_x, angular_y, angular_z;  //角度
	short magnetic_field_x, magnetic_field_y, magnetic_field_z, magnetic_field_h;  //磁场强度
	short angular_velocity_x, angular_velocity_y, angular_velocity_z;  //角速度
	short orientation_x, orientation_y, orientation_z, orientation_w;  //四元数据
	float magenetic_ratio = 0;
	static float s_LastImuAngularZ;
	static unsigned int s_LastTime = 0;
	int RecCount = 0;
	
	if(g_Uart3RxFlag == TRUE)
	{
		if(g_USART3_RX_CNT < 100 && g_USART3_RX_CNT > 40)
		{		
			memcpy(Rec_Buf, g_USART3_RX_BUF, g_USART3_RX_CNT);
			RecCount = g_USART3_RX_CNT;
			Clear_Uart3Buff();
//			u1_printf("Rec3:");
//			for(i=0; i<RecCount; i++)
//			{
//				u1_printf("%02X ", Rec_Buf[i]);
//				
//			}
//			u1_printf("\r\n");
			Imu3TimeOutFlag = FALSE;
			s_LastTime = GetSystem100msCount();
			RecCheckSumAcceleration         = Rec_Buf[10];
			RecCheckSumAngularVelocity      = Rec_Buf[21];
			RecCheckSumAngular              = Rec_Buf[32];
			RecCheckSumAngularMagneticField = Rec_Buf[43];
			RecCheckSumOrientation          = Rec_Buf[54];
			
			for(i=0; i<10; i++) //加速度校验和
			{
				CheckSumAcceleration += Rec_Buf[i];
			}

			for(i=11; i<21; i++) //角速度校验和
			{
				CheckSumAngularVelocity += Rec_Buf[i];
			}

			for(i=22; i<32; i++) //欧拉角校验和
			{
				CheckSumAngular += Rec_Buf[i];
			}

			for(i=33; i<43; i++) //磁场校验和
			{
				CheckSumMagneticField += Rec_Buf[i];
			}

			for(i=44; i<54; i++) //四元数据校验和
			{
				CheckSumOrientation += Rec_Buf[i];
			}
			
			if(((CheckSumAcceleration&0xff) == RecCheckSumAcceleration) && ((CheckSumAngularVelocity&0xff) == RecCheckSumAngularVelocity) && ((CheckSumAngular&0xff) == RecCheckSumAngular) && ((CheckSumMagneticField&0xff) == RecCheckSumAngularMagneticField) && ((CheckSumOrientation&0xff) == RecCheckSumOrientation))
			{
				Imu1StatusFlag = TRUE;
				linear_acceleration_x           = Rec_Buf[2] + (Rec_Buf[3] << 8);
				linear_acceleration_y           = Rec_Buf[4] + (Rec_Buf[5] << 8);
				linear_acceleration_z           = Rec_Buf[6] + (Rec_Buf[7] << 8);

				angular_velocity_x              = Rec_Buf[13] + (Rec_Buf[14] << 8);
				angular_velocity_y              = Rec_Buf[15] + (Rec_Buf[16] << 8);
				angular_velocity_z              = Rec_Buf[17] + (Rec_Buf[18] << 8);


				angular_x                       = Rec_Buf[24] + (Rec_Buf[25] << 8);
				angular_y                       = Rec_Buf[26] + (Rec_Buf[27] << 8);
				angular_z                       = Rec_Buf[28] + (Rec_Buf[29] << 8);


				magnetic_field_x                = Rec_Buf[35] + (Rec_Buf[36] << 8);
				magnetic_field_y                = Rec_Buf[37] + (Rec_Buf[38] << 8);
				magnetic_field_z                = Rec_Buf[39] + (Rec_Buf[40] << 8);

				
				magnetic_field_h = sqrt(magnetic_field_x*magnetic_field_x + magnetic_field_y*magnetic_field_y + magnetic_field_z*magnetic_field_z);
				magenetic_ratio = magnetic_field_h/MEGNETIC_MAX;

				orientation_w                   = Rec_Buf[46] + (Rec_Buf[47] << 8);
				orientation_x                   = Rec_Buf[48] + (Rec_Buf[49] << 8);
				orientation_y                   = Rec_Buf[50] + (Rec_Buf[51] << 8);
				orientation_z                   = Rec_Buf[52] + (Rec_Buf[53] << 8);

				
				ImuData1.orientation_x = orientation_x/32768.0;
				ImuData1.orientation_y = orientation_y/32768.0;
				ImuData1.orientation_z = orientation_z/32768.0;
				ImuData1.orientation_w = orientation_w/32768.0;

				ImuData1.angular_velocity_x = angular_velocity_x/16.384;
				ImuData1.angular_velocity_y = angular_velocity_y/16.384;
				ImuData1.angular_velocity_z = angular_velocity_z/16.384;

				ImuData1.linear_acceleration_x = linear_acceleration_x/208.98;
				ImuData1.linear_acceleration_y = linear_acceleration_y/208.98;
				ImuData1.linear_acceleration_z = linear_acceleration_z/208.98;
		   
				
				ImuData1.angular_x = angular_x/182.044;
				ImuData1.angular_y = angular_y/182.044;
				ImuData1.angular_z = angular_z/182.044;
				
				if(fabs(ImuData1.angular_z - s_LastImuAngularZ) > MAX_ANGULAR_OFFSET)
				{
					s_LastImuAngularZ = ImuData1.angular_z;
					ImuData1.angular_z = s_LastImuAngularZ;
					return;
				}
				s_LastImuAngularZ = ImuData1.angular_z;
				
				if(GetKalmanFilterFlag())
				{
					ImuData1.angular_z = KalmanFilter(ImuData1.angular_z, 5, 10);
				}
				
				ImuData1.magnetic_field_x = magnetic_field_x;
				ImuData1.magnetic_field_y = magnetic_field_y;
				ImuData1.magnetic_field_z = magnetic_field_z;
				
				Imu1_MageneticRatio = magenetic_ratio;
				Count++;
				if(Count >= 10)
				{
					Count = 0;
					
					if(GetShowDataFlag())
					{
						u1_printf("(1)角度:%.2f  %.2f  %.2f  角速度:%.2f  %.2f  %.2f  加速度:%.2f  %.2f  %.2f  %.2f %.2f %.2f %.2f 磁场:%d  干扰:%.2f%% \r\n\r\n",
						ImuData1.angular_x, ImuData1.angular_y, ImuData1.angular_z, \
						ImuData1.angular_velocity_x, ImuData1.angular_velocity_y, ImuData1.angular_velocity_z, \
						ImuData1.linear_acceleration_x, ImuData1.linear_acceleration_y, ImuData1.linear_acceleration_z, \
						ImuData1.orientation_w, ImuData1.orientation_x, ImuData1.orientation_y, ImuData1.orientation_z,\
						magnetic_field_h, magenetic_ratio);
					}
				}
			}
			else
			{
				Imu1StatusFlag = FALSE;
				u1_printf("Imu1 CheckSum Err\r\n");
//				for(i=0; i<RecCount; i++)
//				{
//					u1_printf("%02X ", Rec_Buf[i]);
//					
//				}
//				u1_printf("\r\n");	
			}
			Clear_Uart3Buff();
		}
		else if(g_USART3_RX_CNT > 100)
		{
			Clear_Uart3Buff();
		}
	}
	else
	{
		if(CalculateTime(GetSystem100msCount(), s_LastTime) >= 10)
		{
			s_LastTime = GetSystem100msCount();
			Imu1TimeOutFlag = TRUE;
			Imu1StatusFlag = FALSE;
//			u1_printf(" Imu1 Time out\r\n");
		}
	}
}

void TaskForImu2(void)
{
	int i;
	unsigned char Rec_Buf[100] = {0};

	unsigned char CheckSumAcceleration = 0, CheckSumAngularVelocity = 0, CheckSumAngular = 0, CheckSumMagneticField = 0, CheckSumOrientation = 0;   //计算出的校验和
	unsigned char RecCheckSumAcceleration = 0, RecCheckSumAngularVelocity = 0, RecCheckSumAngular = 0, RecCheckSumAngularMagneticField = 0, RecCheckSumOrientation = 0;  //接收到的校验和
	short linear_acceleration_x, linear_acceleration_y, linear_acceleration_z; //加速度
	short angular_x, angular_y, angular_z;  //角度
	short magnetic_field_x, magnetic_field_y, magnetic_field_z, magnetic_field_h;  //磁场强度
	short angular_velocity_x, angular_velocity_y, angular_velocity_z;  //角速度
	short orientation_x, orientation_y, orientation_z, orientation_w;  //四元数据
	float magenetic_ratio = 0;
	static unsigned int s_LastTime = 0, Count = 0;
	static float s_LastImuAngularZ;
	int RecCount = 0;
	if(g_Uart4RxFlag == TRUE)
	{
		if(g_USART4_RX_CNT < 100 && g_USART4_RX_CNT > 40)
		{		
			memcpy(Rec_Buf, g_USART4_RX_BUF, g_USART4_RX_CNT);
			RecCount = g_USART4_RX_CNT;
			Clear_Uart4Buff();
//			u1_printf("Rec4(%d):", RecCount);
//			for(i=0; i<RecCount; i++)
//			{
//				u1_printf("%02X ", Rec_Buf[i]);
//				
//			}
//			u1_printf("\r\n");
			Imu2TimeOutFlag = FALSE;
			s_LastTime = GetSystem100msCount();
			
			RecCheckSumAcceleration         = Rec_Buf[10];
			RecCheckSumAngularVelocity      = Rec_Buf[21];
			RecCheckSumAngular              = Rec_Buf[32];
			RecCheckSumAngularMagneticField = Rec_Buf[43];
			RecCheckSumOrientation          = Rec_Buf[54];
			
			for(i=0; i<10; i++) //加速度校验和
			{
				CheckSumAcceleration += Rec_Buf[i];
			}

			for(i=11; i<21; i++) //角速度校验和
			{
				CheckSumAngularVelocity += Rec_Buf[i];
			}

			for(i=22; i<32; i++) //欧拉角校验和
			{
				CheckSumAngular += Rec_Buf[i];
			}

			for(i=33; i<43; i++) //磁场校验和
			{
				CheckSumMagneticField += Rec_Buf[i];
			}

			for(i=44; i<54; i++) //四元数据校验和
			{
				CheckSumOrientation += Rec_Buf[i];
			}
			
			if(((CheckSumAcceleration&0xff) == RecCheckSumAcceleration) && ((CheckSumAngularVelocity&0xff) == RecCheckSumAngularVelocity) && ((CheckSumAngular&0xff) == RecCheckSumAngular) && ((CheckSumMagneticField&0xff) == RecCheckSumAngularMagneticField) && ((CheckSumOrientation&0xff) == RecCheckSumOrientation))
			{
				Imu2StatusFlag = TRUE;
				linear_acceleration_x           = Rec_Buf[2] + (Rec_Buf[3] << 8);
				linear_acceleration_y           = Rec_Buf[4] + (Rec_Buf[5] << 8);
				linear_acceleration_z           = Rec_Buf[6] + (Rec_Buf[7] << 8);

				angular_velocity_x              = Rec_Buf[13] + (Rec_Buf[14] << 8);
				angular_velocity_y              = Rec_Buf[15] + (Rec_Buf[16] << 8);
				angular_velocity_z              = Rec_Buf[17] + (Rec_Buf[18] << 8);

				angular_x                       = Rec_Buf[24] + (Rec_Buf[25] << 8);
				angular_y                       = Rec_Buf[26] + (Rec_Buf[27] << 8);
				angular_z                       = Rec_Buf[28] + (Rec_Buf[29] << 8);

				magnetic_field_x                = Rec_Buf[35] + (Rec_Buf[36] << 8);
				magnetic_field_y                = Rec_Buf[37] + (Rec_Buf[38] << 8);
				magnetic_field_z                = Rec_Buf[39] + (Rec_Buf[40] << 8);
				
				magnetic_field_h = sqrt(magnetic_field_x*magnetic_field_x + magnetic_field_y*magnetic_field_y + magnetic_field_z*magnetic_field_z);
				magenetic_ratio = magnetic_field_h/3.0;

				orientation_w                   = Rec_Buf[46] + (Rec_Buf[47] << 8);
				orientation_x                   = Rec_Buf[48] + (Rec_Buf[49] << 8);
				orientation_y                   = Rec_Buf[50] + (Rec_Buf[51] << 8);
				orientation_z                   = Rec_Buf[52] + (Rec_Buf[53] << 8);

				
				ImuData2.orientation_x = orientation_x/32768.0;
				ImuData2.orientation_y = orientation_y/32768.0;
				ImuData2.orientation_z = orientation_z/32768.0;
				ImuData2.orientation_w = orientation_w/32768.0;

				ImuData2.angular_velocity_x = angular_velocity_x/16.384;
				ImuData2.angular_velocity_y = angular_velocity_y/16.384;
				ImuData2.angular_velocity_z = angular_velocity_z/16.384;

				ImuData2.linear_acceleration_x = linear_acceleration_x/208.98;
				ImuData2.linear_acceleration_y = linear_acceleration_y/208.98;
				ImuData2.linear_acceleration_z = linear_acceleration_z/208.98;
		   

				ImuData2.angular_x = angular_x/182.044;
				ImuData2.angular_y = angular_y/182.044;
				ImuData2.angular_z = angular_z/182.044;

				if(fabs(ImuData2.angular_z - s_LastImuAngularZ) > MAX_ANGULAR_OFFSET)
				{
					s_LastImuAngularZ = ImuData2.angular_z;
					ImuData2.angular_z = s_LastImuAngularZ;
					return;
				}
				s_LastImuAngularZ = ImuData2.angular_z;
				
				if(GetKalmanFilterFlag())
				{				
					ImuData2.angular_z = KalmanFilter2(ImuData2.angular_z, 5, 10);
				}
				
				ImuData2.magnetic_field_x = magnetic_field_x;
				ImuData2.magnetic_field_y = magnetic_field_y;
				ImuData2.magnetic_field_z = magnetic_field_z;
				
				Imu2_MageneticRatio = magenetic_ratio;
				Count++;
				if(Count >= 10)
				{
					Count = 0;
					
					if(GetShowDataFlag())
					{
						u1_printf("(2)角度:%.2f  %.2f  %.2f  角速度:%.2f  %.2f  %.2f  加速度:%.2f  %.2f  %.2f  %.2f %.2f %.2f %.2f 磁场:%d  干扰:%.2f%%\r\n\r\n",
						ImuData2.angular_x, ImuData2.angular_y, ImuData2.angular_z, \
						ImuData2.angular_velocity_x, ImuData2.angular_velocity_y, ImuData2.angular_velocity_z, \
						ImuData2.linear_acceleration_x, ImuData2.linear_acceleration_y, ImuData2.linear_acceleration_z, \
						ImuData2.orientation_w, ImuData2.orientation_x, ImuData2.orientation_y, ImuData2.orientation_z,\
						magnetic_field_h, magenetic_ratio);
					}
//					u4_printf("(2)角度:%.2f 干扰:%.2f%%\r\n\r\n",Imu2_Angular_Z, Imu2_MageneticRatio);	
//					sprintf(str, "%.2f", ImuData.angular_x);
//					OLEDFun() -> ShowString(16,4,str);
				}
			}
			else
			{
				Imu2StatusFlag = FALSE;
				u1_printf("Imu2 CheckSum Err\r\n");
//				for(i=0; i<RecCount; i++)
//				{
//					u1_printf("%02X ", Rec_Buf[i]);
//					
//				}
//				u1_printf("\r\n");	
			}	
			Clear_Uart4Buff();
		}
		else if(g_USART4_RX_CNT > 100)
		{
			Clear_Uart4Buff();
		}

		
	}
	else
	{
		if(CalculateTime(GetSystem100msCount(), s_LastTime) >= 10)
		{
			s_LastTime = GetSystem100msCount();
			Imu2TimeOutFlag = TRUE;
			Imu2StatusFlag = FALSE;
//			u1_printf(" Imu2 Time out\r\n");
		}
	}
}



void TaskForImu3(void)
{
	int i;
	unsigned char Rec_Buf[100] = {0};

	static unsigned char Count = 0;
	unsigned char CheckSumAcceleration = 0, CheckSumAngularVelocity = 0, CheckSumAngular = 0, CheckSumMagneticField = 0, CheckSumOrientation = 0;   //计算出的校验和
	unsigned char RecCheckSumAcceleration = 0, RecCheckSumAngularVelocity = 0, RecCheckSumAngular = 0, RecCheckSumAngularMagneticField = 0, RecCheckSumOrientation = 0;  //接收到的校验和
	short linear_acceleration_x, linear_acceleration_y, linear_acceleration_z; //加速度
	short angular_x, angular_y, angular_z;  //角度
	short magnetic_field_x, magnetic_field_y, magnetic_field_z, magnetic_field_h;  //磁场强度
	short angular_velocity_x, angular_velocity_y, angular_velocity_z;  //角速度
	short orientation_x, orientation_y, orientation_z, orientation_w;  //四元数据
	float magenetic_ratio = 0;
	static float s_LastImuAngularZ;
	static unsigned int s_LastTime = 0;
	unsigned int RecCount = 0;
	
	if(g_Uart2RxFlag == TRUE)
	{
		if(g_USART2_RX_CNT < 100 && g_USART2_RX_CNT > 40)
		{		
			memcpy(Rec_Buf, g_USART2_RX_BUF, g_USART2_RX_CNT);
			RecCount = g_USART2_RX_CNT;
			Clear_Uart2Buff();
			
//			u1_printf("Rec2(%d):", RecCount);
//			for(i=0; i<RecCount; i++)
//			{
//				u1_printf("%02X ", Rec_Buf[i]);
//				
//			}
//			u1_printf("\r\n");
			
			Imu3TimeOutFlag = FALSE;
			s_LastTime = GetSystem100msCount();
			RecCheckSumAcceleration         = Rec_Buf[10];
			RecCheckSumAngularVelocity      = Rec_Buf[21];
			RecCheckSumAngular              = Rec_Buf[32];
			RecCheckSumAngularMagneticField = Rec_Buf[43];
			RecCheckSumOrientation          = Rec_Buf[54];
			
			for(i=0; i<10; i++) //加速度校验和
			{
				CheckSumAcceleration += Rec_Buf[i];
			}

			for(i=11; i<21; i++) //角速度校验和
			{
				CheckSumAngularVelocity += Rec_Buf[i];
			}

			for(i=22; i<32; i++) //欧拉角校验和
			{
				CheckSumAngular += Rec_Buf[i];
			}

			for(i=33; i<43; i++) //磁场校验和
			{
				CheckSumMagneticField += Rec_Buf[i];
			}

			for(i=44; i<54; i++) //四元数据校验和
			{
				CheckSumOrientation += Rec_Buf[i];
			}
			
			if(((CheckSumAcceleration&0xff) == RecCheckSumAcceleration) && ((CheckSumAngularVelocity&0xff) == RecCheckSumAngularVelocity) && ((CheckSumAngular&0xff) == RecCheckSumAngular) && ((CheckSumMagneticField&0xff) == RecCheckSumAngularMagneticField) && ((CheckSumOrientation&0xff) == RecCheckSumOrientation))
			{
				Imu3StatusFlag = TRUE;
				linear_acceleration_x           = Rec_Buf[2] + (Rec_Buf[3] << 8);
				linear_acceleration_y           = Rec_Buf[4] + (Rec_Buf[5] << 8);
				linear_acceleration_z           = Rec_Buf[6] + (Rec_Buf[7] << 8);

				angular_velocity_x              = Rec_Buf[13] + (Rec_Buf[14] << 8);
				angular_velocity_y              = Rec_Buf[15] + (Rec_Buf[16] << 8);
				angular_velocity_z              = Rec_Buf[17] + (Rec_Buf[18] << 8);


				angular_x                       = Rec_Buf[24] + (Rec_Buf[25] << 8);
				angular_y                       = Rec_Buf[26] + (Rec_Buf[27] << 8);
				angular_z                       = Rec_Buf[28] + (Rec_Buf[29] << 8);


				magnetic_field_x                = Rec_Buf[35] + (Rec_Buf[36] << 8);
				magnetic_field_y                = Rec_Buf[37] + (Rec_Buf[38] << 8);
				magnetic_field_z                = Rec_Buf[39] + (Rec_Buf[40] << 8);

				
				magnetic_field_h = sqrt(magnetic_field_x*magnetic_field_x + magnetic_field_y*magnetic_field_y + magnetic_field_z*magnetic_field_z);
				magenetic_ratio = magnetic_field_h/3.0;

				orientation_w                   = Rec_Buf[46] + (Rec_Buf[47] << 8);
				orientation_x                   = Rec_Buf[48] + (Rec_Buf[49] << 8);
				orientation_y                   = Rec_Buf[50] + (Rec_Buf[51] << 8);
				orientation_z                   = Rec_Buf[52] + (Rec_Buf[53] << 8);

				
				ImuData3.orientation_x = orientation_x/32768.0;
				ImuData3.orientation_y = orientation_y/32768.0;
				ImuData3.orientation_z = orientation_z/32768.0;
				ImuData3.orientation_w = orientation_w/32768.0;

				ImuData3.angular_velocity_x = angular_velocity_x/16.384;
				ImuData3.angular_velocity_y = angular_velocity_y/16.384;
				ImuData3.angular_velocity_z = angular_velocity_z/16.384;

				ImuData3.linear_acceleration_x = linear_acceleration_x/208.98;
				ImuData3.linear_acceleration_y = linear_acceleration_y/208.98;
				ImuData3.linear_acceleration_z = linear_acceleration_z/208.98;
		   
				
				ImuData3.angular_x = angular_x/182.044;
				ImuData3.angular_y = angular_y/182.044;
				ImuData3.angular_z = angular_z/182.044;
				
				if(fabs(ImuData3.angular_z - s_LastImuAngularZ) > MAX_ANGULAR_OFFSET)
				{
					s_LastImuAngularZ = ImuData3.angular_z;
					ImuData3.angular_z = s_LastImuAngularZ;
					return;
				}
				s_LastImuAngularZ = ImuData3.angular_z;
				
				if(GetKalmanFilterFlag())
				{
					ImuData3.angular_z = KalmanFilter3(ImuData3.angular_z, 5, 10);
				}
				
				ImuData3.magnetic_field_x = magnetic_field_x;
				ImuData3.magnetic_field_y = magnetic_field_y;
				ImuData3.magnetic_field_z = magnetic_field_z;
				
				Imu3_MageneticRatio = magenetic_ratio;
				Count++;
				if(Count >= 10)
				{
					Count = 0;
					
					if(GetShowDataFlag())
					{
						u1_printf("(3)角度:%.2f  %.2f  %.2f  角速度:%.2f  %.2f  %.2f  加速度:%.2f  %.2f  %.2f  %.2f %.2f %.2f %.2f 磁场:%d  干扰:%.2f%%\r\n\r\n",
						ImuData3.angular_x, ImuData3.angular_y, ImuData3.angular_z, \
						ImuData3.angular_velocity_x, ImuData3.angular_velocity_y, ImuData3.angular_velocity_z, \
						ImuData3.linear_acceleration_x, ImuData3.linear_acceleration_y, ImuData3.linear_acceleration_z, \
						ImuData3.orientation_w, ImuData3.orientation_x, ImuData3.orientation_y, ImuData3.orientation_z,\
						magnetic_field_h, magenetic_ratio);
					}			
//					u4_printf("(3)角度:%.2f 干扰:%.2f%%\r\n\r\n",Imu3_Angular_Z, Imu3_MageneticRatio);	
//					sprintf(str, "%.2f", ImuData.angular_x);
//					OLEDFun() -> ShowString(16,6,str);
				}
			}
			else
			{
				Imu3StatusFlag = FALSE;
				u1_printf("Imu3 CheckSum Err(%d)\r\n", RecCount);	
//				for(i=0; i<RecCount; i++)
//				{
//					u1_printf("%02X ", Rec_Buf[i]);
//					
//				}
//				u1_printf("\r\n");				
			}	
		}
		else if(g_USART2_RX_CNT > 100)
		{
			u1_printf(" U2 Over\r\n");
			Clear_Uart2Buff();
		}
		
	}
	else
	{
		if(CalculateTime(GetSystem100msCount(), s_LastTime) >= 10)
		{
			s_LastTime = GetSystem100msCount();
			Imu3TimeOutFlag = TRUE;
			Imu3StatusFlag = FALSE;
//			u1_printf(" Imu3 Time out\r\n");
		}
	}
}

void TaskForImuFuse(void)
{
	static unsigned char SendBuf[100] = {0},  Count = 0;
	unsigned char CheckSum = 0, i;
	float Temp = 0, Temp1 = 0, Temp2 = 0, Temp3 = 0;
	float val_float = 0;
	static unsigned int s_LastTime = 0;
	unsigned char ImuCount = 0;		//需要计算的Imu数量
	
	
	if((Imu1StatusFlag == TRUE) && (Imu2StatusFlag == TRUE) && (Imu3StatusFlag == TRUE))	//找出3个IMU中收到干扰最小的
	{
		if((Imu1_MageneticRatio > 100) && (Imu2_MageneticRatio > 100) && (Imu3_MageneticRatio > 100))
		{
			if(Imu1_MageneticRatio > Imu2_MageneticRatio)
			{
				if(Imu1_MageneticRatio > Imu3_MageneticRatio)
				{
					Imu2StatusFlag = FALSE;
					Imu3StatusFlag = FALSE;
				}
				else
				{
					Imu1StatusFlag = FALSE;
					Imu2StatusFlag = FALSE;
				}
			}
			else
			{
				if(Imu2_MageneticRatio > Imu3_MageneticRatio)
				{
					Imu1StatusFlag = FALSE;
					Imu3StatusFlag = FALSE;
				}
				else
				{
					Imu1StatusFlag = FALSE;
					Imu2StatusFlag = FALSE;
				}
			}
		}
	}

	memset(&ImuDataOut, 0, sizeof(ImuDataOut));
	ImuCount = 0;
	
	if(Imu1StatusFlag)
	{
		ImuDataOut.angular_x += ImuData1.angular_x;
		ImuDataOut.angular_y += ImuData1.angular_y;
		
		ImuDataOut.angular_velocity_x += ImuData1.angular_velocity_x;
		ImuDataOut.angular_velocity_y += ImuData1.angular_velocity_y;
		ImuDataOut.angular_velocity_z += ImuData1.angular_velocity_z;
		
		ImuDataOut.linear_acceleration_x += ImuData1.linear_acceleration_x;
		ImuDataOut.linear_acceleration_y += ImuData1.linear_acceleration_y;
		ImuDataOut.linear_acceleration_z += ImuData1.linear_acceleration_z;
		
		ImuDataOut.orientation_w += ImuData1.orientation_w;
		ImuDataOut.orientation_x += ImuData1.orientation_x;
		ImuDataOut.orientation_y += ImuData1.orientation_y;
		ImuDataOut.orientation_z += ImuData1.orientation_z;
		
		ImuDataOut.MageneticRatio += Imu1_MageneticRatio;
		
		ImuCount++;
	}
	
	if(Imu2StatusFlag)
	{
		ImuDataOut.angular_x += ImuData2.angular_x;
		ImuDataOut.angular_y += ImuData2.angular_y;
				
		ImuDataOut.angular_velocity_x += ImuData2.angular_velocity_x;
		ImuDataOut.angular_velocity_y += ImuData2.angular_velocity_y;
		ImuDataOut.angular_velocity_z += ImuData2.angular_velocity_z;
		
		ImuDataOut.linear_acceleration_x += ImuData2.linear_acceleration_x;
		ImuDataOut.linear_acceleration_y += ImuData2.linear_acceleration_y;
		ImuDataOut.linear_acceleration_z += ImuData2.linear_acceleration_z;
		
		ImuDataOut.orientation_w += ImuData2.orientation_w;
		ImuDataOut.orientation_x += ImuData2.orientation_x;
		ImuDataOut.orientation_y += ImuData2.orientation_y;
		ImuDataOut.orientation_z += ImuData2.orientation_z;
		
		ImuDataOut.MageneticRatio += Imu2_MageneticRatio;
		
		ImuCount++;
	}
	
	if(Imu3StatusFlag)
	{
		ImuDataOut.angular_x += ImuData3.angular_x;
		ImuDataOut.angular_y += ImuData3.angular_y;
	
		ImuDataOut.angular_velocity_x += ImuData3.angular_velocity_x;
		ImuDataOut.angular_velocity_y += ImuData3.angular_velocity_y;
		ImuDataOut.angular_velocity_z += ImuData3.angular_velocity_z;
		
		ImuDataOut.linear_acceleration_x += ImuData3.linear_acceleration_x;
		ImuDataOut.linear_acceleration_y += ImuData3.linear_acceleration_y;
		ImuDataOut.linear_acceleration_z += ImuData3.linear_acceleration_z;
		
		ImuDataOut.orientation_w += ImuData3.orientation_w;
		ImuDataOut.orientation_x += ImuData3.orientation_x;
		ImuDataOut.orientation_y += ImuData3.orientation_y;
		ImuDataOut.orientation_z += ImuData3.orientation_z;
		
		ImuDataOut.MageneticRatio += Imu3_MageneticRatio;
		
		ImuCount++;
	}
	
	
	if((fabs(ImuData1.angular_z) >= 170) || (fabs(ImuData2.angular_z) >= 170) || (fabs(ImuData3.angular_z) >= 170))	//Z方向角度接近+-180°时需进行特殊处理
	{
		if(Imu1StatusFlag)
		{
			if(ImuData1.angular_z >= 0)//正数
			{
				Temp1 = 180 - ImuData1.angular_z;
			}
			else
			{
				Temp1 = -180 - ImuData1.angular_z;
			}
		}

		
		if(Imu2StatusFlag)
		{
			if(ImuData2.angular_z >= 0)//正数
			{
				Temp2 = 180 - ImuData2.angular_z;
			}
			else
			{
				Temp2 = -180 - ImuData2.angular_z;
			}
		}
		
		if(Imu3StatusFlag)
		{
			if(ImuData3.angular_z >= 0)//正数
			{
				Temp3 = 180 - ImuData3.angular_z;
			}
			else
			{
				Temp3 = -180 - ImuData3.angular_z;
			}
		}
		
		Temp = 0;
		
		if(Imu1StatusFlag)
		{
			Temp += Temp1;
		}
		if(Imu2StatusFlag)
		{
			Temp += Temp2;
		}
		if(Imu3StatusFlag)
		{
			Temp += Temp3;
		}
		
		if(Temp >= 0)
		{
			ImuDataOut.angular_z = 180 - Temp/ImuCount;
		}
		else
		{
			ImuDataOut.angular_z = -180 - Temp/ImuCount;
		}
	}
	else
	{
		ImuDataOut.angular_z = 0;
		if(Imu1StatusFlag)
		{
			ImuDataOut.angular_z += ImuData1.angular_z;
		}
		if(Imu2StatusFlag)
		{
			ImuDataOut.angular_z += ImuData2.angular_z;
		}
		if(Imu3StatusFlag)
		{
			ImuDataOut.angular_z += ImuData3.angular_z;
		}	
		ImuDataOut.angular_z = ImuDataOut.angular_z/(float)ImuCount;
	}
	
	ImuDataOut.angular_x = ImuDataOut.angular_x/(float)ImuCount;
	ImuDataOut.angular_y = ImuDataOut.angular_y/(float)ImuCount;
	
	ImuDataOut.angular_velocity_x = ImuDataOut.angular_velocity_x/(float)ImuCount;
	ImuDataOut.angular_velocity_y = ImuDataOut.angular_velocity_y/(float)ImuCount;
	ImuDataOut.angular_velocity_z = ImuDataOut.angular_velocity_z/(float)ImuCount;
	
	ImuDataOut.linear_acceleration_x = ImuDataOut.linear_acceleration_x/(float)ImuCount;
	ImuDataOut.linear_acceleration_y = ImuDataOut.linear_acceleration_y/(float)ImuCount;
	ImuDataOut.linear_acceleration_z = ImuDataOut.linear_acceleration_z/(float)ImuCount;
	
	ImuDataOut.orientation_w = ImuDataOut.orientation_w/(float)ImuCount;
	ImuDataOut.orientation_x = ImuDataOut.orientation_x/(float)ImuCount;
	ImuDataOut.orientation_y = ImuDataOut.orientation_y/(float)ImuCount;
	ImuDataOut.orientation_z = ImuDataOut.orientation_z/(float)ImuCount;
	
	ImuDataOut.MageneticRatio = ImuDataOut.MageneticRatio/(float)ImuCount;
	
	ImuDataSave = ImuDataOut;
	
	if(GetShowDataFlag())
	{
		Count++;
		if(Count >= 10)
		{
			Count = 0;
			u1_printf("(%d%d%d)角度:%.2f  %.2f  %.2f  角速度:%.2f  %.2f  %.2f  加速度:%.2f  %.2f  %.2f 四元:%.3f %.3f %.3f %.3f\r\n\r\n", Imu1StatusFlag, Imu2StatusFlag, Imu3StatusFlag, \
			ImuDataOut.angular_x, ImuDataOut.angular_y, ImuDataOut.angular_z, \
			ImuDataOut.angular_velocity_x, ImuDataOut.angular_velocity_y, ImuDataOut.angular_velocity_z, \
			ImuDataOut.linear_acceleration_x, ImuDataOut.linear_acceleration_y, ImuDataOut.linear_acceleration_z, \
			ImuDataOut.orientation_w, ImuDataOut.orientation_x, ImuDataOut.orientation_y, ImuDataOut.orientation_z);
		}
	}
					
	if(Imu1_MageneticRatio > 100 || Imu2_MageneticRatio > 100 || Imu3_MageneticRatio > 100)
	{
		POWER1_CONTROL_ON();
	}
	else
	{
		POWER1_CONTROL_OFF();
	}
}

void ImuDetectInit(void)
{
	memset(&ImuDataOut, 0, sizeof(ImuDataOut));
	
	Task_Create(TaskForImu1, 1);
	
	Task_Create(TaskForImu2, 1);
	
	Task_Create(TaskForImu3, 1);
	
	Task_Create(TaskForImuFuse, 4000);
}











