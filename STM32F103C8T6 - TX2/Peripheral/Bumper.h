#ifndef		_BUMPER_H_

#define		_BUMPER_H_


#define		BUMPER_LEFT_GPIO_TYPE		GPIOA
#define		BUMPER_LEFT_GPIO_PIN		GPIO_Pin_6

#define		BUMPER_RIGHT_GPIO_TYPE		GPIOB
#define		BUMPER_RIGHT_GPIO_PIN		GPIO_Pin_0


void Bumper_Sensor_GPIO_Init(void);

void TaskForBumperSensor(void);

unsigned char GetBumperRightFlag(void);

unsigned char GetBumperLeftFlag(void);



#endif	
