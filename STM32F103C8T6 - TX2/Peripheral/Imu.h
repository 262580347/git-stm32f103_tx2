#ifndef		_IMU_H_
#define		_IMU_H_

typedef struct
{
	float linear_acceleration_x, linear_acceleration_y, linear_acceleration_z; //加速度
	float angular_x, angular_y, angular_z;  //角度
	float magnetic_field_x, magnetic_field_y, magnetic_field_z, magnetic_field_h;  //磁场强度
	float angular_velocity_x, angular_velocity_y, angular_velocity_z;  //角速度
	float orientation_x, orientation_y, orientation_z, orientation_w;  //四元数据
	float MageneticRatio;  //磁场干扰百分比
}ImuType;

void ImuDetectInit(void);

ImuType* GetImuDataOut(void);

#endif


