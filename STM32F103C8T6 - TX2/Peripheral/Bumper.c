#include "sys.h"
#include "Bumper.h"

static unsigned char s_BumperRightFlag = FALSE;
static unsigned char s_BumperLeftFlag = FALSE;

unsigned char GetBumperRightFlag(void)
{
	return s_BumperRightFlag;
}

unsigned char GetBumperLeftFlag(void)
{
	return s_BumperLeftFlag;
}

void Bumper_Sensor_GPIO_Init(void)
{

	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = BUMPER_LEFT_GPIO_PIN;	       
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;      
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(BUMPER_LEFT_GPIO_TYPE, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = BUMPER_RIGHT_GPIO_PIN;	           
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;     
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;    
	GPIO_Init(BUMPER_RIGHT_GPIO_TYPE, &GPIO_InitStructure);
	
}

void TaskForBumperSensor(void)
{
	static unsigned int s_LastTime = 0, s_LastTime2;
	static unsigned int s_RightCount = 0, s_LeftCount = 0;
	
	if(CalculateTime(GetSystem100msCount(), s_LastTime) >= 1)
	{
		s_LastTime = GetSystem100msCount();
		
		if(GPIO_ReadInputDataBit(BUMPER_LEFT_GPIO_TYPE, BUMPER_LEFT_GPIO_PIN) == Bit_RESET)
		{
			s_BumperLeftFlag = TRUE;
			s_LeftCount++;
			
			if(s_LeftCount < 5)
			{		
				ControlStop();
//				u1_printf("Stop Left\r\n");
			}
		}
		else
		{
			s_BumperLeftFlag = FALSE;
			s_LeftCount = 0;
		}
		
		if(GPIO_ReadInputDataBit(BUMPER_RIGHT_GPIO_TYPE, BUMPER_RIGHT_GPIO_PIN) == Bit_RESET)
		{
			s_BumperRightFlag = TRUE;
			
			s_RightCount++;
			if(s_RightCount < 5)
			{
				ControlStop();
//				u1_printf("Stop Right\r\n");
			}
		}
		else
		{
			s_BumperRightFlag = FALSE;
			s_RightCount = 0;
		}
	}
	
	if(CalculateTime(GetSystem100msCount(), s_LastTime2) >= 10)
	{
		s_LastTime2 = GetSystem100msCount();
//		u1_printf("L:%d R:%d\r\n", s_BumperLeftFlag, s_BumperRightFlag);
		
	}
}





















