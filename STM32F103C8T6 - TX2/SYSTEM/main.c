#include "sys.h"

//delay_ms(500);	//延时不要超过1800,否则会溢出


int main(void)
{ 
	SysInit();
	
	Task_Create(TaskForDebugCOM, 10);
	
	Task_Create(TaskFor2812LED, 4000);
	
	Task_Create(TaskForBumperSensor, 50);
	
	SetTimer(200, GetMpu6050Data);	//读取时间不能太长
	
	while(1)
	{	
		Task_Execute();
	} 
}

